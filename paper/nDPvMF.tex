\documentclass[letterpaper]{article}
\usepackage{nips15submit_e,times}
\usepackage{hyperref}
\usepackage{url}
%\documentstyle[nips14submit_09,times,art10]{article} % For LaTeX 2.09

%\usepackage{nips14submit_e,times}
\input{eqn_abbr}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{natbib}
\usepackage{hyperref}
\usepackage{url}
\usepackage{breqn}
\usepackage{refstyle}
\usepackage{graphicx}
%\usepackage{subfigure} 
\usepackage{caption}
\usepackage{subcaption}
\usepackage{appendix}
\usepackage{chngcntr}

\usepackage{algpseudocode}
\usepackage[]{algorithm2e}

\newcommand{\ie}{{\sl i.e.}}
\newcommand{\eg}{{\sl e.g.}}
\newcommand{\etc}{{\sl etc.}}
\newcommand{\etal}{{\sl et al.}}

\allowdisplaybreaks

\title{Hierarchical and Temporal von Mises-Fisher Clustering}
%
\author{
Sachinthaka Abeywardana, Didi Surian, Sanjay Chawla, Fabio Ramos \\
School of Information Technologies, The University of Sydney, Australia\\
\texttt{sachinra@it.usyd.edu.au, \{didi.surian, sanjay.chawla, fabio.ramos\}@sydney.edu.au}
%\And
%
%School of Information Technologies, The University of Sydney, Australia\\
%National ICT Australia \\
%\texttt{\{didi.surian,sanjay.chawla\}@sydney.edu.au} \\
}

%\nipsfinalcopy

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

%\newcommand{\fix}{\marginpar{FIX}}
%\newcommand{\new}{\marginpar{NEW}}

%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle

\begin{abstract}
This paper addresses a problem of clustering directional (normalised) data using the von Mises-Fisher (vMF) distribution which is a more appropriate setting in many application domains including text analysis. We propose: a) Dirichlet Process von Mises-Fisher (DP-vMF) as a non-parametric approach, b) nested Dirichlet Process von Mises-Fisher (nDP-vMF) which, extends DP-vMF to handle hierarchical data, and c) temporal-vMF framework (t-vMF) to handle time series data. We develop efficient Variational Bayesian methods for the posterior inference. Our experiments on a variety of datasets showcase our approaches.
\end{abstract}

\section{Introduction}
Extracting latent patterns has been one of main focuses in data mining and machine learning communities. The problem of extracting latent patterns has been studied extensively in many application domains such as document analysis~\cite{jeongt10,NIPS2010_4094}, user behaviour~\cite{AhmedLAJS11}, \etc.  One of the prominent models which is extensively studied is Latent Dirichlet Allocation model (LDA)~\cite{blei2003latent}. Initially, LDA was proposed to analyse the latent structure of a corpus of documents. LDA is a probabilistic generative model that assumes an instance in the data (\eg\ a word in text data) is represented by a discrete feature-counts and is generated from a multinomial distribution. However, discrete count-based methods may not be appropriate to be used to model data in some domains. For example, text data is commonly represented by its normalised form using tf-idf (term frequency inverse document frequency)~\cite{salton}. By taking the normalised form, our analysis is not biased by the length of documents as we put more importance to the directions of the normalised vector which represent the data.

In recent years, von Mises-Fisher (vMF) distribution has been gaining its popularity to model directional data. The vMF distribution is one of distributions studied in directional statistics \cite{MardiaJ00,FisherLE87}. The probability density function of vMF distribution for a $d$-dimensional unit vector $x$ is: $p(x|\mu,\kappa)=\frac{\kappa^\nu}{(2\pi)^{d/2}I_\nu(\kappa)}\exp(\kappa\cmu^T x)$, where $\nu=d/2-1$ and $I_\nu(\cdot)$ is the modified Bessel function. $\mu$ represents the mode (not mean, refer to Appendix \ref{app:evmf}) direction and $\kappa$ is analogous to a precision variable. As $\kappa\to 0$ the distribution moves towards a uniform distribution over the hyper-sphere and on contrary, as $\kappa\to\infty$ the distribution concentrates to a point with the direction $\mu$. In contrast the Gaussian distribution is commonly used to model real valued data. In a vMF distribution the direction plays more of an important role. Image classification would be one such task; a certain image frame would contain the same information regardless of how bright or dull the image may be.

\cite{gopal2014mises,taghia2014bayesian,banerjee2005clustering,anh2013document} are the most prominent work in vMF clustering. The difficulty in all of these models has been to model the `precision' variable $\kappa$. \cite{gopal2014mises} uses vMF clustering for topic modelling. This work uses two methods on modelling the posterior for $\kappa$ one being sampling which is in general slower than Variational Bayes (VB) methods. The second method being an attempt to use a lower bound on $\log I_\nu(\kappa)$. However, due to the approximation used on top of this lower bound no guarantee is given to show that it is still a lower bound. \cite{taghia2014bayesian} incorrectly states that $\log I_\nu(\kappa)$ is strictly concave w.r.t. $\kappa$, hence, using an incorrect lower bound (proof in Appendix \ref{app:concave}). \cite{banerjee2005clustering} like the other two frameworks has a restricted number of clusters. Even though \cite{anh2013document} uses a Dirichlet Process (DP) to automate the number of clusters a full Bayesian treatment is not applied as a M-step is used to calculate the most likely $\kappa$ variable. Furthermore, it does not address the issue of converging to a local maxima as is often the case with VB algorithms. 

Although the hierarchical aspect of data has been studied previously, the implementation of vMF distribution in this case is less explored to explain the generative process of hierarchical data. Previous studies on hierarchical structure have proposed approaches such as systems based on Pachinko Allocation Model~\cite{lim06pachinko}, tree-structured stick breaking process~\cite{ghahramanija10tssb}, nested Chinese Restaurant Process~\cite{griffiths2004hierarchical}, recursive Chinese Restaurant Process~\cite{kim12rcrp}, \etc. Despite the success in modelling hierarchical data using those proposed approaches, none have considered the directional aspect of data. The main contributions that are presented in this paper are:
\begin{enumerate}
\item We show a provable lower bound to $-\log I_\nu(\kappa)$.
\item The numbers of clusters are automatically determined and this number is free to grow as more observations are made.
\item A nested architecture to deal with hierarchical models.
\item A more intuitive way of dealing with time series vMF models as compared to \cite{taghia2014bayesian}.
\end{enumerate}


This paper is structured as follows: In \Secref{BI} the Bayesian generative structure is presented for our proposed three frameworks. This includes the approximate VB posteriors which are inferred during these processes. Experiments that were conducted to validate these methods are presented in \Secref{exp} and final concluding remarks are given in \Secref{conc}.

\section{Bayesian Inference}\label{sec:BI}
\subsection{Dirichlet Process}

Dirichlet Processes, DP($\alpha,H$)), where $\alpha$ is a positive constant and $H$ is a base distribution, has been used to model infinite cluster models. DPs are distributions over distributions (\cite{sudderth2006graphical}(Section 2.5),\cite{ferguson1973bayesian}). The expected number of clusters in a DP grows proportional to $\log(N)$ where, $N$ is the number of observations. Variational inference in such a setting is usually done by truncating the ``infinite'' nature of the process. In order to describe a DP, consider the generative process: An infinite number of parameters, $\theta_i$ are sampled from a base distribution $H$. An infinite number of 'remaining' stick lengths, $V_i$ are drawn from a beta distribution ($Be(1,\alpha)$). This generates a stick breaking process whose i-th stick has length $V_i\prod_{j=1}^{i-1}(1-V_j)$. This gives rise to an infinite multinomial distribution whose parameters are the lengths of the sticks.  The multinomial distribution randomly selects a (hyper) parameter $\theta_i$ per observation. Finally, the observation is generated from a predetermined distribution $p(\cx_n|\theta_{z_n})$.

The goal of this Bayesian framework is to infer the posterior, $p(\cz|\cx)$. However, an appropriate analytic distribution cannot be found. Markov Chain Monte Carlo (MCMC) methods are popular inference techniques for Bayesian models. However, due to the sampling nature of MCMC routines, they tend to be slow and is especially a problem when the number of latent variables are large. In this construction, we focus on Variational Bayes (VB) methods \cite{tzikas2008variational} which have shown to be significantly faster than MCMC. A variational distribution $q(l_i)$ is used for each latent variable $l_i$ such that $q(\bigcup l_i;\theta)=\prod q_i(l_i)\approx p(\bigcup l_i|\cx,\theta)$. The variational distributions are chosen such that it maximises the lower bound $\mathcal{L}(\bigcup q(l_i))$ on the true log likelihood $\log p(\cx)$. It can be shown by using coordinate ascent that for each variational distribution $\log q_i(l_i) \propto E_{q(l_{/i})}\clrbracket{\log p(\cy | l_i,l_{/i},\theta)+\log p(l_i)}$ where $l_{/i}$ is all latent variables but the i-th and, the expectation $E$ is taken with respect to all $q(l_j)$ but the i-th. Due to the dependence of the i-th variational distribution on the rest of the variational distributions, this algorithm is iterated until convergence.

\subsection{Dirichlet Process vMF Clustering (DP-vMF)}
\label{sec:dpvmf}
%A large number of $\{\mu_i,\kappa_i\}$ are jointly generated by the base distributions. Unlike traditional DPs in this occasion two base distributions are used. Finally $z_n$ chooses the parameter pair, $\{\mu_i,\kappa_i\}$ and \eqref{gen} generates the data.
%\begin{align}
%\alpha \sim & \, Ga(10^{-6},10^{-6})
%\label{eq:alpha}\\
%V_i|\alpha \sim & \, Be(1,\alpha)
%\label{eq:V}\\
%\mu_i|\mu_0,\kappa_0\sim & \, vMF(\mu_0\kappa_0)\quad i\in\{1,...,\infty\}\\
%\kappa_i|a,b\sim & \, Ga(10^{-6},10^{-6}) \quad i\in\{1,...,\infty\}\\
%z_n|\mathbf{V}\sim & \, Mult(\pi(\mathbf{(V)})) \quad n\in\{1,...,N\} \\
%\mathbf{x}_n|z_n\sim & \, vMF(\mu_{z_n},\kappa_{z_n}) \quad n\in\{1,...,N\}
%\label{eq:gen}
%\end{align}
\begin{figure}[h]
\centering
\begin{subfigure}{0.3\textwidth}
\centering\includegraphics[height=2.5in]{DPvMF_graphs}
\caption{}
\label{fig:DPvMF_graph}
\end{subfigure}  
\begin{subfigure}{0.6\textwidth}
\centering\includegraphics[height=2.5in]{nDPvMF_graphs}
\caption{}
\label{fig:nDPvMF_graph}
\end{subfigure} 
\caption{Graphical models for the (a): DP-vMF and (b): nDP-vMF with three levels. All $\pi$'s are generated from a stick breaking process. $\cx_n$ is generated by $\prod_{t=1}^{T}\text{vMF}(\cx_n|\mu_t^{(L)},\kappa_t^{(L)})^{1(z_n=t)}$ where $N$ is the number of observations, $T$ is the number of clusters which could theoretically be infinite but is truncated and, $L$ is the final level. $\{\mu_t^{(1)},\kappa_t^{(1)}\}$ are the parameters that are generated from the base distributions vMF($\mu_t|\mu_0,10^{-6}$) and Ga$(10^{-6},10^{-6})$. In nDP-vMF it is only $\mu_t^{(l)}$ of level $l$ that is generated from $\prod_{t=1}^{T}\text{vMF}(\mu_t^{(l-1)},\kappa_t^{(l-1)})^{1(z_n^{(l)}=t)}$ and not $\kappa_t^{(l)}$ which is simply generated from Ga$(10^{-6},10^{-6})$. The number of levels $L$ can be trivially extended to an arbitrary number.}
\end{figure}
The generative process for DP-vMF is shown in \Figref{DPvMF_graph}. The difficulty that has been encountered in this framework and other frameworks that uses a vMF distribution \footnote{In this paper we use the notation $vMF(\theta)$ to describe the distribution. The length of the vector $\lVert\theta\rVert$ is the precision variable, while $\theta/\lVert\theta\rVert$ is the direction associated with the traditional notation.} is the posterior on the precision variable $\kappa$. The complication is due to the dependence on the log Bessel function, $\log I_\nu (\kappa)$ (where $\nu=\frac{d}{2}-1$ and $d$ is the dimension of the data) when performing VB inference. To circumvent this issue, a lower bound is used on $f(\kappa)=\nu\log\kappa-\log I_\nu(\kappa)$ which is the log normalising constant of a vMF distribution, disregarding constants. \cite{balachandran2013exponential} states that, $$ f'(\kappa)=-\frac{I_{\nu+1}(\kappa)}{I_\nu(\kappa)}>-\sqrt{1+\clrbracket{\frac{\nu+1/2}{\kappa}}^2}+\frac{\nu+1/2}{\kappa}$$ Integrating both sides we obtain the lower bound for $\nu\log\kappa-\log I_\nu(\kappa)$,
\begin{align}
f(\kappa)>-\sqrt{\kappa_t^2+C^2}+C\log\clrbracket{C+\sqrt{C^2+\kappa_t^2}}+C\log C>-\kappa+C\log(\kappa_t)+const
%\nonumber >&-\sqrt{\kappa_t^2+C^2}+C\log\clrbracket{1+\sqrt{1+(\kappa_t/C)^2}}+2C\log C\\
%\nonumber>&-\kappa-C+C\log(\kappa_t/C)+2C\log C\\
%>&-\kappa+C\log(\kappa_t)+const
\label{eq:lb}
\end{align}  
where $C=(d-1)/2$. The derivations of the approximate posteriors can be found in Appendix \ref{app:dpvmf_inf}. The approximate posteriors for $q(\alpha)$ and $q(V_t)$ can be found in \cite{blei2006variational}. The novel posteriors are as follows:
\begin{align}
%q(\alpha) = & \, Ga\clrbracket{T+10^{-6},10^{-6}-\sum_{i=1}^{T} \clrangle{\log (1-V_i)}}\\
%q(V_t) = & \, Be\clrbracket{\sum_{n=1}^{N} q(z_n=t)+1,\sum_{n=1}^{N} q(z_n>t)+\clrangle{\alpha}} \\
q(z_n=t) \propto & \, \exp\clrbracket{\clrangle{\log vMF(\cx_n|\mu_{t},\kappa_{t})}+\clrangle{\log(V_t)}+\sum_{i=1}^{t-1}\clrangle{\log(1-V_i)}} \\
q(\mu_t) = & \, vMF\clrbracket{\clrangle{\kappa_{t}}\sum_{n=1}^{N} q(z_n=t) \cx_n+\kappa_0\mu_0}\\
q(\kappa_t) = & \, Ga\clrbracket{C\sum_{n=1}^{N}q(z_n=t)+10^{-6},\sum_{n=1}^{N}q(z_n=t)\clrbracket{1-\clrangle{\mu_{t}}^Tx_n}+10^{-6}}
\end{align}

One of the short-comings of VB is that it tends to get stuck in local maxima. This is mainly due to using coordinate ascent as the optimisation method. To sidestep this issue, a few random starts are used and the iteration with the highest lower bound to $\log p(\cx)$, $\clrangle{\log p(\cx,\cz,\cmu,\ckappa,\cVV,\alpha)-\log q(\cz)q(\cmu)q(\ckappa)q(\cVV)q(\alpha)}$ is chosen. This expectation is with regards to all the approximate posteriors. Note however, that the latent variables which do not contribute towards data creation, denoted by $\csumn q(z_n=t)=0$ are pruned out of this lower bound.

Some of the inferences made on the latent variables can be intuitively interpreted. It is important to note that the mode direction of $q(\mu_t)$ is comprised of all the data vectors that are likely to be in that cluster (which is measured by $q(z_n=t)$). The expectation on the precision variable is, $$\clrangle{\kappa_t}=\frac{C\sum_{n=1}^{N}q(z_n=t)+10^{-6}}{\sum_{n=1}^{N}q(z_n=t)\clrbracket{1-\clrangle{\mu_{t}}^Tx_n}+10^{-6}}$$
What is so significant about this is that, if there is a high chance that this cluster is occupied ($\sum_{n=1}^{N}q(z_n=t)$), the expectation increases. It is further escalated if the mode direction aligns with the data ($1-\clrangle{\mu_{t}}^Tx_n$) by minimising the denominator. The fact that this expectation is obtained by using the lower bound in \eqref{lb} is further validity to its relevance in this framework. Finally, in reference to the assignment variable, $z_n$ the likelihood that it assigns observation $n$ to cluster $t$ depends on two factors. Firstly, the likelihood is higher if $\cx_n$ is aligned with $\mu_t$ through the expectation $\clrangle{\log vMF(\cx_n|\cmu_t,\kappa_t)}$. However, this factor is dampened by the (proxy to) stick length $\exp\clrbracket{\clrangle{\log(V_t)}+\sum_{i=1}^{t-1}\clrangle{\log(1-V_i)}}$. The smaller the latent stick length $V_t\prod_{i=1}^{t-1}(1-V_i)\in(0,1)$ the higher the suppression of cluster $t$. This is a major difference between this framework and the previous work in this area. As more and more evidence is gathered it may cause the suppression or addition of clusters as needed.

We could extend this framework to do semi-supervised clustering by setting $q(z_n=t)=1$ and not updating these distributions for the training set. Semi-supervised clustering gives some labels of the available data and the system would be required to learn the label of incoming data. A semi-supervised system would be able to classify some of the data as completely new clusters instead of using existing ones. A good characteristic of DPs is its ability to add more clusters as more data becomes available. This semi-supervised paradigm could be trivially extended to nDP-vMF and t-vMF.

\subsection{Nested Dirichlet Process vMF Clustering (nDP-vMF)}\label{sec:ndpvmf}

We extend the generative process introduced in \Secref{dpvmf} to handle a hierarchical clustering in \Figref{nDPvMF_graph}. Note how $z^{(l)}_n$ chooses the cluster from the previous level and the parameter pair $\{\mu^{(l-1)}_t,\kappa^{(l-1)}_t\}$ becomes the hyper-parameters for the current level. Finally, the data is generated at the $(L+1)$th level. Similarly, the posteriors for the nDP-vMF are as follows (derivation can be seen in Appendix \ref{app:ndpvmf_inf}):
\begin{align}
q\left(\mu_{t}^{(l)}\right) = & vMF\clrbracket{\sum_{j=1}^{\infty}\pi^{(l)}_{t,j}\clrangle{\kappa^{(l-1)}_{j}{\mu^{(l-1)}_{j}}^T}+\clrangle{\kappa^{(l)}_{t}}\sum_{n=1}^{\infty}\pi^{(l+1)}_{n,t}\clrangle{{\mu^{(l+1)}_n}^T}}\\
q\left(\kappa^{(l)}_{t}\right)= & Ga\clrbracket{C\sum_{n=1}^{\infty}\pi^{(l+1)}_{n,t}+10^{-6}, \sum_{n=1}^{\infty}\pi^{(l+1)}_{n,t}\clrbracket{1-\clrangle{{\mu^{(l)}_{t}}^T\cmu^{(l+1)}_n}}+10^{-6}}
\end{align}
where $\pi^{(l)}_{n,j}\equiv q(z^{(l)}_n=j)$ and $n(l)=t$. Replace $\mu^{(l+1)}_{t}$ with $x_t$ if $l=L$ since this is the last layer. Also other posteriors are exactly the same as that for its single layer counter part. 

It should be noted that these VB updates are iterated in a bottom up approach. For each iteration the posteriors for level $L$ is updated followed by $L-1$ and so on. This is done so that the observed nodes ($\cx$) affects the posteriors straight away and leads to faster convergence.

%As before the algorithm is iterated a few times with random restarts and the highest likelihood is chosen. Pruning unused $\mu^{(l)}_{t},\kappa^{(l)}_{t}$ from the lower bound in level $L$ is evident by ignoring latent variables  which cause $\csumn q(z^{(L)}_{n}=t)=0$. However, due to initialising unused $\mu^{(l)}_{t}$, it is not a simple matter of pruning clusters  with  $\csumt q(z^{(L-1)}_{n}=t)=0$ for the level above. Instead we prune latent variables with $sumcol(Q_L\times Q_{L-1},t)=0$ where, the operator $sumcol(A,t)$ sums the column $t$ of matrix $A$ and $Q_{l}$ is the matrix of level $l$ with elements $Q_{l,ij}=q(z^{(l)}_{i}=j)$. For level $L-2$ it would be $sumcol(Q_L\times Q_{L-1}\times Q_{L-2},t)=0$ and so on. 

The posterior on $\kappa^{(l)}_{t}$ is similar to that of the single level. The more that cluster $t$ is likely to be occupied at the \emph{next} level ($\sum_{n=1}^{\infty}\pi^{(l+1)}_{n,t}$), the higher the precision. Furthermore, the alignment $\clrangle{{\mu^{(l)}_t}^T\cmu^{(l+1)}_{n}}$ is of interest as opposed to $\clrangle{\mu_{t}^T}\cx_n$. The alignment of $\mu_t^{(l)}$ with its probable children, measured by $\pi^{(l+1)}_{n,t}$ will cause the expected precision to increase.

%Specifically alignment of the current expected direction of cluster $t$ at level $l$ ($\mu^{(l)}_t$) with expected directions of level $l+1$ and also belong to cluster $t$ (which is measured by $q(z^{(l+1)}_n=t)$) will cause the expected precision $\clrangle{\kappa^{(l)}_{t}}$ to rise. 

The posterior on $\mu^{(l)}_{t}$ is affected by two parts. Firstly, $\clrangle{\kappa^{(l)}_{t}}\sum_{n=1}^{\infty}\pi^{(l+1)}_{n,t}\clrangle{{\mu^{(l+1)}_n}^T}$ averages out all the latent variables/observations (from the level below) that belong to cluster $t$. This is similar to the single level clustering algorithm. However, the mode direction is also affected by $\sum_{j=1}^{\infty}\pi^{(l)}_{t,j}\clrangle{\kappa^{(l-1)}_{j}{\mu^{(l-1)}_{j}}^T}$. This term sums up all possible mode directions from the level above that could have possibly generated the current latent variable. However, in general it should be noted that the contribution from the level is much smaller than the contribution from the latent variables in the level below. This is due to the fact that $\clrangle{\kappa^{(l)}_{t}}>\clrangle{\kappa^{(l-1)}_{j}}\forall j$. This phenomenon is observed due to the fact that variance of the clusters at lower levels grow smaller as the resolution is increased getting closer to the observations. 

%The posterior on $\mu^{(l)}_{t}$ is affected by two parts. Firstly, $\clrangle{\kappa^{(l)}_{t}}\sum_{n=1}^{\infty}\pi^{(l+1)}_{n,t}\clrangle{{\mu^{(l+1)}_n}^T}$ averages out all the latent variables/ observations (from the level below) that belong to cluster $t$ ($\pi^{(l+1)}_{n,t}$) are summed. This is similar to the single level clustering algorithm. However, the mode direction is also affected by $\sum_{j=1}^{\infty}\pi^{(l)}_{t,j}\clrangle{\kappa^{(l-1)}_{j}{\mu^{(l-1)}_{j}}^T}$. This term sums up all possible mode directions from the level above ($\mu^{(l-1)}_{j}$) that could have possibly generated the current latent variable. $q(z^{(l)}_n)=j$ measures this `possibility' of belonging to cluster $j$. However, in general it should be noted that the contribution from the level is much smaller than the contribution from the latent variables in the level below. 



\subsection{Temporal vMF (t-vMF)}

The generative process differs from a DP-vMF in that we use $z_n$ and an additional Bernoulli variable, $w_n$ to choose the generative cluster. This probability $p$ that $\mathbf{w_n}=1$ is set to a high value such that there is a higher chance of the data being generated from the previous cluster ($\mu_{z_n-1}$) than another cluster ($\mu_{z_n}$). This process is named a sticky DP (\cite{fox2008hdp}) for its tendency to 'stick' to the previous observed cluster. In the temporal framework presented in \cite{gopal2014mises} the clusters parameters seem to be evolving with time and there is no consistency parameter that would `tend' to draw the parameters from the same previous cluster.
\begin{align}
z_n|\mathbf{V}\sim & \, Mult(\pi(\mathbf{(V)})) \quad n\in\{1,...,N\} \\
w_n\sim & p \quad n\in\{1,...,N\}\\
\mathbf{x}_n|w_n,z_n\sim & \, vMF(\mu_{z_{n-1}},\kappa_{z_{n-1}})^{\mathbf{w_n}} vMF(\mu_{z_n},\kappa_{z_n})^{\mathbf{1-w_n}} \quad n\in\{1,...,N\}
\end{align}
In order to get the predicted class, as opposed to looking for the class which has the maximum $q(z_n=t)$, we take the class that has the maximum value of $q(w_n=1)q(z_{n-1}=t)+q(w_n=0)q(z_{n}=t)$. The new approximate posteriors obtained are as follows:
\begin{align}
\nonumber q(w_n)\propto&\exp\clrbracket{\log p +\sum_{t=1}^{T}\pi_{n-1,t}\clrangle{\log p(\cx_n|\theta_t)}}^{w_n}\\
&\qquad\exp\clrbracket{\log (1-p) +\sum_{t=1}^{T}\pi_{n,t}\clrangle{\log p(\cx_n|\theta_t)}}^{1-w_n}\\
\pi_{n,t} \propto & \, \exp\clrbracket{S_t+\clrangle{1-w_n}\clrangle{\log p(\cx_n|\theta_{t})}+\clrangle{w_{n+1}}\clrangle{\log p(\cx_{n+1}|\theta_{t})}}\\
q(\cmu_t) = & vMF\clrbracket{\clrangle{\kappa_t}\csumn\pi_{2,n,t}\cx_n+10^{-6}\mathbf{1}^T}\\
q(\kappa_t) = & \, Ga\clrbracket{\frac{d-1}{2}\sum_{n=1}^{N} \pi_{2,n,t}+10^{-6},\sum_{n=1}^{N}C_{n,t}\clrbracket{1-\clrangle{\mu_{t}}^Tx_n}+10^{-6}}
\end{align}
where $\pi_{2,n,t}=\clrangle{1-w_n}\pi_{n,t}+\clrangle{w_n}\pi_{n-1,t}$, $S_t$ is the expected log stick length $\clrangle{\log(V_t)}+\sum_{i=1}^{t-1}\clrangle{\log(1-V_i)}$ and $\theta_t=\{\mu_t,\kappa_t\}$. The weighted term, $\pi_{2,n,t}$ is the probability that $n$ belongs to cluster $t$.

The posterior on $\kappa_t$ and $\mu_t$ is similar to the previous two frameworks except that it is affected by the weighted probability $\pi_{2,n,t}$ instead. The posterior on $z_n$ is affected not only by the current data point but also the next, $\clrangle{\log p(\cx_{n+1}|\theta_{t})}$. Thus the likelihood that $n$ belongs to cluster $t$ increases not only if $\cx_n$ aligns with $\mu_t$ but also $\cx_{n+1}$. 


\section{Experiments}
\label{sec:exp}
We have used various datasets to test the efficacy of our framework. We performed our experiments\footnote{All code would be made available after review at \url{www.bitbucket.org/____/dirProcess}.} on several clustering tasks: 1) gene clustering, 2) topic modelling on a corpus of documents, and 3) action recognition. All data sources are given in Appendix \ref{app:sources}. We also used a synthetic dataset to show the performance of our approach. We compared the performance of our framework with the methods presented in \cite{gopal2014mises,taghia2014bayesian} as well as the well-known \textit{k}-means method. We evaluated the performance using the metrics of log likelihood (or the lower bound), Normalised Mutual Index (NMI) and Adjusted Random Index (ARI) when labels are available and Homogeneity/Separability measures when labels are unknown(refer to Appendix~\ref{app:metrics} for more details).

\subsection{Synthetic Data}
We begin the experiments by showing proof of concept by clustering on a 3-d sphere. Four clusters of 50 observations were generated with a $\kappa$ value of 50. The true (unnormalised) directions were as follows: $[1\quad 1\quad 1]^T [-1\, -1\,-1]^T [-0.5\quad 4\quad1]^T [-2\quad 0.5\,-1]^T$. \Figref{syn_inf} shows that DP-vMF properly inferred the 4 clusters. The dot product of the inferred mode directions and the true directions were as follows: $0.9998\quad 0.9999\quad 0.9995\quad 1.0000$.

\begin{figure}[h]
\centering
\begin{subfigure}{0.4\textwidth}
\centering\includegraphics[height=1in]{synthetic_sphere}
\caption{Original data}
\label{fig:syn}
\end{subfigure} \qquad  
\begin{subfigure}{0.4\textwidth}
\centering\includegraphics[height=1in]{synthetic_sphere_inf}
\caption{Clustered data}
\label{fig:syn_inf}
\end{subfigure}
\caption{Clustering synthetic data. a) Original data were generated using $\kappa$ value = 50 from a vMF distribution, b) The clusters that were inferred by DP-vMF.}\label{fig:synthetic}
\end{figure}

\vspace{-0.5cm}
\subsection{Topic Modelling}
Topic modelling has been a prominent area where clustering algorithms have been used extensively. Latent Dirichlet Allocation (LDA)~\cite{blei2003latent} has been the most significant work in this area which modelled word frequencies with Dirichlet distributions. However, it has been shown that representing a document using its term frequency - inverse document frequency (tf-idf) representation gives superior performance than using term frequency histogram only. The idf component gives higher weighting to the rare terms by taking into account the fraction of documents that contain term $t$.

We compared our method against B-vMFmix~\cite{gopal2014mises} (which has been shown to have better performance than \cite{banerjee2005clustering}), \textit{k}-means and \textit{k}-means2 where the distance is calculated using a dot product. We used three datasets: CNAE , K1 and News20. Table~\ref{tab:tfidf_res} shows the results of our experiments. For \textit{k}-means and \textit{k}-means2, we used two approaches: by defining the number of clusters directly (shown as the number inside the parentheses) and by taking the number of clusters inferred by our DP-vMF (shown as (DP)). For \textit{k}-means2, we also tested using the true number of clusters (shown as (T)). The results show that \textit{k}-means2 outperforms \textit{k}-means indicating that the length of the vector plays an insignificant role in clustering documents. We compared the semi supervised method against \textit{k}-means2 (T) (where the true number of clusters are given to the algorithm). Our algorithm is able to learn the structure of clusters and gives superior performance when no labels are known and to \textit{k}-means2 where the correct number of clusters are given. 

The only exception as seen in the 	\Tabref{tfidf_res} is for the unsupervised clustering of News20. News20-Trunc is a truncated dataset where any terms that have a total sum of less than 2 across documents were discarded. This cuts the vocabulary (dimension) of the dataset from 53,975 words to 12,968. As can be seen our method does not perform even as well as \textit{k}-means2(DP). This is due to the fact that considering such a large vocabulary that is sparse and spread out a precision parameter plays little role in separating clusters. This claim is further strengthened by the fact that our algorithm surpasses \cite{gopal2014mises} for the NMI metric when the truncated dataset is considered. We did one final experiment where we clustered a set of NIPS papers. The results of this clustering can be seen in \Figref{tfidf}. The log likelihood of all topic clustering experiments are shown in \Tabref{tfidf_log}.

%\begin{table}[ht]
%\centering
%\begin{tabular}{lllllllll}
%  \hline
% & \multicolumn{2}{c}{CNAE} & \multicolumn{2}{c}{K1} & \multicolumn{2}{c}{News20} & \multicolumn{2}{c}{News20-Trunc}\\ 
% & NMI & ARI  & NMI & ARI  & NMI & ARI & NMI & ARI\\
%  \hline
%  DP-vMF-SS & \bf{0.8507} & \bf{0.8137} & \bf{0.6793} & \bf{0.6135} & \bf{0.7342} & \bf{0.6725} \\ 
%  \textit{k}-Means2(T) & 0.7895 & 0.6819 & 0.5895 & 0.3491 & 0.5665 & 0.3888 \\ \hline
%  DP-vMF & \bf{0.7713} & \bf{0.6691} & \bf{0.5626} & \bf{0.4279} & 0.4843 & 0.2162 \\ 
%  \textit{k}-Means(30) & 0.585 & 0.3156 & 0.4875 & 0.2115 & 0.4881 & 0.2042 \\ 
%  \textit{k}-Means2(DP) & 0.717 & 0.5843 & 0.5043 & 0.2474 & 0.5687 & 0.3512 \\ 
%  \textit{k}-Means2(5) & 0.602 & 0.3773 & 0.4839 & 0.218 & 0.402 & 0.1672 \\ 
%  B-vMFmix & 0.748 & 0.669 & 0.551 & 0.352 & \bf{0.567} & \bf{0.397} \\ 
%   \hline
%\end{tabular}
%\caption{Comparison of NMI and ARI across different methods of clustering. First two rows represent semi supervised methods where half of the labels were given.}
%\label{tab:tfidf_res}
%\end{table}

% Wed Jun  3 19:38:53 2015
\begin{table}[ht]
\centering
\begin{tabular}{lrrrrrrrr}
  \hline
& \multicolumn{2}{c}{CNAE} & \multicolumn{2}{c}{K1} & \multicolumn{2}{c}{News20} & \multicolumn{2}{c}{News20-Trunc}\\ \hline
 & NMI & ARI  & NMI & ARI  & NMI & ARI & NMI & ARI\\ 
  \hline
DP-vMF-SS & \bf{\bf{0.8507}} & \bf{\bf{0.8137}} & \bf{\bf{0.6793}} & \bf{\bf{0.6135}} & \bf{\bf{0.7342}} & \bf{\bf{0.6725}} & \bf{0.717} & \bf{0.6432} \\ 
  \textit{k}-means2(T) & 0.7895 & 0.6819 & 0.5895 & 0.3491 & 0.5665 & 0.3888 & 0.575 & 0.4022 \\
  \hline 
  DP-vMF & \bf{\bf{0.7713}} & \bf{\bf{0.6691}} & \bf{\bf{0.5626}} & \bf{\bf{0.4279}} & 0.4843 & 0.2162 & 0.5709 & 0.3845 \\ 
  \textit{k}-means(30) & 0.5850 & 0.3156 & 0.4875 & 0.2115 & 0.4881 & 0.2042 & 0.4347 & 0.1674 \\ 
  \textit{k}-means2(DP) & 0.7170 & 0.5843 & 0.5043 & 0.2474 & \bf{\bf{0.5687}} & 0.3512 & \bf{0.5750} & \bf{0.4225} \\ 
  \textit{k}-means2(5) & 0.6020 & 0.3773 & 0.4839 & 0.2180 & 0.4020 & 0.1672 & 0.4466 & 0.1991 \\ 
  B-vMFmix & 0.748 & 0.669 & 0.551 & 0.352 & 0.567 & \bf{\bf{0.397}} & - & - \\ 
   \hline
\end{tabular}
\caption{Comparison of NMI and ARI across different methods of clustering. First two rows represent semi supervised methods where half of the labels were given.}
\label{tab:tfidf_res}
\end{table}



\begin{figure}[ht]
\centering
\begin{subfigure}{0.3\textwidth}
\centering\includegraphics[width=0.8in]{chips}
\end{subfigure}  
\begin{subfigure}{0.3\textwidth}
\centering\includegraphics[width=0.8in]{bayes}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
\centering\includegraphics[width=0.8in]{network_training}
\end{subfigure}
%\begin{subfigure}{0.3\textwidth}
%\centering\includegraphics[width=1.5in]{neuron}
%\end{subfigure}  
%\begin{subfigure}{0.3\textwidth}
%\centering\includegraphics[width=1.5in]{brenner}
%\end{subfigure}
%\begin{subfigure}{0.3\textwidth}
%\centering\includegraphics[width=1.5in]{lewicki}
%\end{subfigure}
\caption{Top 10 words of the three inferred clusters. Size determines the weighting of the word. Note that each word is a dimension. The largest 10 dimensions are portrayed. All of the six inferred clusters are shown in \Figref{tfidf_app} (Appendix).}
\label{fig:tfidf}
\end{figure} 

\begin{table}[h]
\centering
\begin{tabular}{lrrrr}
\hline
  & CNAE & K1 & News20  & NIPS\\
\hline  
DP-vMF &$\mathbf{2.53 \times 10^6}$ & $\mathbf{2.17 \times 10^8}$ & $\mathbf{4.63 \times 10^9}$ & $\mathbf{1.42 \times 10^8}$\\
B-vMFmix &$0.94 \times 10^6$ & $0.92 \times 10^8$ & $1.63 \times 10^9$ & $0.58\times 10^8$ \\ \hline
\end{tabular}
\caption{Comparison of log likelihood lower bounds across text clustering problems.}
\label{tab:tfidf_log}
\end{table}

\vspace{-0.3cm}
\subsection{Genetics}
Clustering genes is advantageous in understanding genetic interaction and how they work together to create certain proteins. A yeast gene dataset which contains 4,382 genes across 23 experiments was used. In this section, we explore the strengths of hierarchical clustering.

The Pearson correlation coefficient ($\rho(x,y)=\tilde{x}^T\tilde{y}$) is commonly used to measure similarity where, $\tilde{x}=\frac{x-\bar{x}}{||x-\bar{x}||}$. $\bar{x}$ is calculated by averaging across experiments. Furthermore, $||\tilde{x}||_2^2=1$. This points out that any Bayesian clustering done using the Pearson's correlation coefficient should be carried out using the vMF distribution. 

In this instance, it is useful not only to understand the interaction between genes themselves, but also the interaction between clusters. A hierarchical clustering approach is thus suitable for this problem and is presented in \Figref{yeast3}.

\begin{figure}[h]
\centering
\begin{subfigure}{0.3\textwidth}
\centering\includegraphics[height=1in]{yeast}
\caption{Original data}
\label{fig:yeast_o}
\end{subfigure}  
\begin{subfigure}{0.3\textwidth}
\centering\includegraphics[height=1in]{yeast2}
\caption{Single level clustering}
\label{fig:yeast2}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
\centering\includegraphics[height=1in]{yeast3}
\caption{Multi level clustering}
\label{fig:yeast3}
\end{subfigure}
\caption{Reordering genes by relevant cluster. The vertical axis represents the genes while the horizontal axis are the different experiments. Multi level clustering is used to improve upon single level clustering. Note how the strands of similar colours are better visible under the nDP-vMF. }
\label{fig:yeast}
\end{figure}

We used the measures of Average Separability ($S_{ave}$), Maximum Separability ($S_{max}$) (both smaller the better) and Average Homogeneity ($H_{ave}$), Minimum Homogeneity ($H_{min}$) to evaluate the performance of this nested clustering. In summary we need the homogeneity measurements to be maximised while the separability measure should be  minimal for a good clustering. At the lowest of the clustering, 90 clusters were inferred while 6 clusters were inferred in the level above. No code was available to test the algorithm by \cite{taghia2014bayesian}. However, we followed the same procedure in testing our algorithm against \cite{banerjee2005clustering}'s algorithm and can be visually compared against the results by \citep[Figure~9]{taghia2014bayesian}. \cite{taghia2014bayesian} recorded 22 clusters by initiating 30 clusters and disregarding the clusters that were not picked. It should be noted that the author did not attempt to repeat the algorithm by initiating the clusters with different numbers of initial clusters. Conversely we were able to infer 90 clusters without the need for setting the number of clusters. DP-vMF inferred 86 clusters.

\begin{figure}[h]
\centering
\begin{subfigure}{0.2\textwidth}
\centering\includegraphics[height=0.8in]{H_min}
\caption{$H_{min}$}
\label{fig:H_min}
\end{subfigure}  
\begin{subfigure}{0.2\textwidth}
\centering\includegraphics[height=0.8in]{H_ave}
\caption{$H_{ave}$}
\label{fig:H_ave}
\end{subfigure}
\begin{subfigure}{0.2\textwidth}
\centering\includegraphics[height=0.8in]{S_max}
\caption{$S_{max}$}
\label{fig:S_max}
\end{subfigure}
\begin{subfigure}{0.2\textwidth}
\centering\includegraphics[height=0.8in]{S_ave}
\caption{$S_{ave}$}
\label{fig:S_ave}
\end{subfigure}
\caption{Comparison of nested clustering vs. single level clustering of \cite{banerjee2005clustering}. The blue line represents the similarity measure obtained by \cite{banerjee2005clustering} over a given number of clusters, while the red dot is the measure obtained by our algorithm which infers the number of clusters automatically. The higher the better for $H$ measures and the lower the better for $S$ measures.}
\label{fig:HS}
\end{figure}

%\vspace{-0.5cm}
\subsection{Activity Recognition}
The final experiment is on an activity recognition task, where a phone's accelerometer data was collected and six actions were performed. The goal of this task was to demonstrate the performance of t-vMF clustering. We show that the accuracy of clustering is improved by incorporating the temporal information. The dataset consists of 7,352 instances and each instance is a 561 dimensional feature vector. In \Tabref{tVMFSS} the initial 500 instances were used as a training set.

\begin{table}[h]
\centering
\begin{subtable}{0.4\textwidth}
	\centering
	\begin{tabular}{lrr}
		\hline
		& NMI & ARI \\
		\hline
		vMF & $0.5910$ & $0.4047$ \\
		t-VMF & $\mathbf{0.6073}$ & $\mathbf{0.4398}$ \\
		\hline
	\end{tabular}
	\caption{Unsupervised Clustering.}
	\label{tab:tVMF}
\end{subtable}
\begin{subtable}{0.4\textwidth}
	\centering
	\begin{tabular}{lrr}
		\hline
		& NMI & ARI \\
		\hline
		vMF-SS & $0.5950$ & $0.4765$\\
		t-VMF-SS & $\mathbf{0.5986}$ & $\mathbf{0.4803}$\\
		\hline
	\end{tabular}
	\caption{Semi-supervised Clustering.}
	\label{tab:tVMFSS}
\end{subtable}
\caption{Comparison of clustering when temporal consistency information is used. The -SS suffix represents the semi-supervised setting.}
\label{tab:vMFt}
\end{table}

In the supervised setting, the accuracy improves from 0.6125 to 0.6219 when the time consistency condition is included. The $p$ parameter was set to 0.9 in both the supervised and unsupervised cases. In the first 500 instances the label changed only 21 times thus, indicating that 95.8\% of the time the label was consistent. Setting $p=0.958$ only improved the accuracy to 0.6222 and the NMI to 0.5987 while the ARI remained the same. This indicates that the algorithm is not overly sensitive to the value of $p$.

\vspace{-0.1cm}
\section{Conclusion}
\label{sec:conc}
\vspace{-0.2cm}
In this paper we have proposed solutions for three variations of the clustering problem when data is generated from a vMF distribution. We have used the stick breaking version of a Dirichlet process to automatically determine the number of clusters in the context of vMF. For hierarchical data, we have a used a nested version of DPs.
Finally for temporal data, we have modelled the auto-regressive nature data using a "sticky version" of DP. Our experiments, on diverse data sets, affirm that vMF distribution  results in clusters of better quality. For future work we would like to explore the connections between DP-vMF and Independent Component Analysis (ICA).

%\clearpage
%\small{
\bibliographystyle{plain} 
\bibliography{DirProcessRef}
%}
\input{appendix2}
\end{document}