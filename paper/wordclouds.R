setwd('~/thesis/dirProcess/paper/')

library(wordcloud)
weights=read.csv('weights.csv',header=FALSE)
topics=read.csv('topics.csv',header=FALSE)


for (i in 1:6)
wordcloud(topics[i,],weights[i,]*100,colors=brewer.pal(8,"Dark2"), scale=c(5,0.5), max.words=100, random.order=FALSE, rot.per=0.35, use.r.layout=FALSE)
