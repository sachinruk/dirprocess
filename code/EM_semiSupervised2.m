function [phi_z_best,class_best,Emu_best,L_best]=EM_semiSupervised2(x,labels,pos)

%%%%%%%%%%%%%%%%%%%%%%%
%Inputs:
% x- test and train set combined
% labels- I will assume first part of x corresponds to labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


T=100; [N,d]=size(x); train_set=length(labels);

% figure;
L_best=-inf;
restarts=80;

phi_z_train=zeros(train_set,T);
%     phi_z(1:train_set+(labels-1)*train_set)=1;
phi_z_train(sub2ind(size(phi_z_train), (1:train_set)', labels)) = 1;
x_train=x(1:train_set,:);
x_test=x((train_set+1):end,:);
C=(d-1)/2;
class_len=length(unique(labels));
Emu_train=mean_dir(x_train,labels);
    
for iter=1:restarts    
    phi_z=[phi_z_train; ones(N-train_set,T)/T];
    Emu=[Emu_train; rand_dir(T-class_len,d,pos)];
    Ealpha=class_len+5; %assuming upto 5 more classes
    L=zeros(15,1);
    factor=ones(T,1);
    u=zeros(T,1)+50;
    for i=1:100        
        [~, ~,~,kappa_b]=q_kappa(x,phi_z,Emu,C,factor);
        [factor, u]=q_exp_ut(u,kappa_b,phi_z,C);
        [Ekappa, Elnkappa,kappa_a,kappa_b]=q_kappa(x,phi_z,Emu,C,factor);      
        [Emu,mu_kappa]=q_mu(x,phi_z,Ekappa);
        [lnV, ln1_V,V_a,V_b]=qV(phi_z,Ealpha);
        [Ealpha, a,b]=q_alpha(ln1_V);     
        phi_z=qz(x_test,lnV,ln1_V,Emu,Ekappa,Elnkappa,phi_z_train,u,C);
        L(i)=lnlb4(x,a,b,Ealpha,V_a,V_b,lnV,ln1_V,mu_kappa,Emu,kappa_a,kappa_b,Ekappa,Elnkappa,u,phi_z);
        if i>1
            if (i>15 && abs(L(i)-L(i-1))<1e-5) || (i>15 && abs(L(i)-L(i-1))/L(i-1)<1e-7)
                break;
            end
        end
    end
%     [~,class]=max(phi_z,[],2);
%     subplot(8,10,iter);plot(L); xlabel(length(unique(class)));
    if L(end)>L_best
        L_best=L(end);
        phi_z_best=phi_z;
%         class_best=class;
        Emu_best=Emu;
    end    
end

[~,class_best]=max(phi_z_best,[],2);
Emu_best=Emu_best(:,unique(class_best,'stable'));
class_best=relabel(class_best);
Emu_best=bsxfun(@rdivide,Emu_best,sqrt(sum(Emu_best.^2)));

%gamma distribution approximation
function [Ealpha, a,b]=q_alpha(ln1_V)
a=length(ln1_V)+1e-9;
b=1e-9-sum(ln1_V);
Ealpha=a/b;
% Elnalpha=psi(a)-log(b);

%beta distribution approximation
function [lnV, ln1_V, gamma_1,gamma_2]=qV(phi_z,Ealpha)
gamma_1=(sum(phi_z)+1)';
phi_z=cumsum(phi_z,2);
gamma_2=(sum(1-phi_z)+Ealpha)';
% gamma=[gamma_1 gamma_2];

%expectations
lnV=psi(gamma_1)-psi(gamma_1+gamma_2);
ln1_V=psi(gamma_2)-psi(gamma_1+gamma_2);

%multinomail approxmiation
function p=qz(x,lnV,ln1_V,Emu,Ekappa,Elnkappa,phi_z_train,u,C)
ln1_V=cumsum(ln1_V);
ln1_V=[0; ln1_V(1:(end-1))];

ln_exp_u_term=log(1+sqrt(1+exp(u)));
extreme_idx=u>500;
ln_exp_u_term(extreme_idx)=u(extreme_idx)/2;
factor=1-1./(sqrt(exp(u)+1));
c_kappa=-Ekappa+C*ln_exp_u_term+C*factor.*(Elnkappa-log(C)-0.5*u);

% lnVMF=bsxfun(@plus,C2,Ekappa'),-Ekappasq'/(2*(d-2)));
lnVMF=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),c_kappa');

p=bsxfun(@plus,lnVMF,(ln1_V+lnV)');
p=exp(bsxfun(@minus,p,max(p,[],2)));
p=bsxfun(@rdivide, p,sum(p,2));
p=[phi_z_train; p];

%vMF approxmiation
function [Emu,eta_kappa]=q_mu(x,p,Ekappa)
%prior parameters
kappa=1e-9; d=size(x,2); 
% T=length(Ekappa); 
mu=1./sqrt(d);
prior_const=kappa*mu;
v=bsxfun(@times,x'*p,Ekappa')+prior_const;
eta_kappa=sqrt(sum(v.^2));
eta_mu=bsxfun(@rdivide,v,eta_kappa);
% Emu=v/(d-2);
% C=besseli(d/2,eta_kappa)./besseli(d/2-1,eta_kappa);

lb=sqrt(1+((d/2)./eta_kappa).^2)-(d/2)./eta_kappa; 
ub=sqrt(1+((d/2-0.5)./eta_kappa).^2)-(d/2-0.5)./eta_kappa;
C_est=0.5*(lb+ub);
% C(isnan(C))=C_est(isnan(C));

Emu=bsxfun(@times,eta_mu,C_est)';

%gamma approximation on kappa
function [Ekappa, Elnkappa,a,b]=q_kappa(x,p,mu,C,factor)
a=C*sum(p)'; 
a=a.*factor+1e-9;
b=sum((1-x*mu').*p)'+1e-9; % C2=sum((p'*x)'.*mu)';<- can be used instead
Ekappa=a./b;
Elnkappa=psi(a)-log(b); 

function [factor,u]=q_exp_ut2(Elnkappa,C)
u=2*(Elnkappa-log(C));

factor=1-1./sqrt(exp(u)+1);
extreme_idx=u>500;
factor(extreme_idx)=1;