%lower bound to likelihood
function [L]=lnlb3(x,a,b,Ealpha, V_a,V_b,lnV,ln1_V, mu_kappa,Emu,...
                                Ekappa,Elnkappa,c_kappa,lnqkappa, phi_z)
% a=length(ln1_V)+1e-6;
% b=1e-6-sum(ln1_V);
lnpalpha=-1e-6*Ealpha;
lnqalpha=a*log(b)-gammaln(a)-b*Ealpha;
lb_alpha=lnpalpha-lnqalpha;
% Ealpha=a/b;

occ_clusters=(sum(phi_z)>0); %occupied clusters

gamma_1=(sum(phi_z))';
phi_z2=cumsum(phi_z,2);
phi_z2=bsxfun(@minus,phi_z2(:,end),phi_z2);
gamma_2=sum(phi_z2)';
lnpV=gamma_1.*lnV+gamma_2.*ln1_V+(Ealpha-1)*ln1_V;
lnqV=gammaln(V_a+V_b)-(gammaln(V_a)+gammaln(V_b))+(V_a-1).*lnV+(V_b-1).*ln1_V;
lb_V=lnpV-lnqV;
% lb_V=sum(lb_V(occ_clusters));
lb_V=sum(lb_V);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lgIeta_kappa=log(besseli(nu,eta_kappa));
% lgIeta_kappa_large=eta_kappa-0.5*log(2*pi*eta_kappa); %for large argument
% lgIeta_kappa(eta_kappa>200)=lgIeta_kappa_large(eta_kappa>200);
% lb_mu=sum(lgIeta_kappa-nu*log(eta_kappa));
p=size(x,2); nu=p/2-1; kappa=1e-6; d=size(x,2); mu=1./sqrt(d); prior_const=kappa*mu;
lnpmu=sum(prior_const*Emu');
sqrt_part=sqrt(mu_kappa.^2+(nu+1).^2);
C1=sqrt_part-(nu+1);
C1(C1==0)=mu_kappa(C1==0).^2;
lb_lnqmu=sqrt_part+(nu+1)*log(C1)-2*(nu+1)*log(mu_kappa); %verified

%mu part of q(mu)
C=besseli(d/2,mu_kappa)./besseli(d/2-1,mu_kappa);
lb=sqrt(1+((d/2)./mu_kappa).^2)-(d/2)./mu_kappa; 
ub=sqrt(1+((d/2-0.5)./mu_kappa).^2)-(d/2-0.5)./mu_kappa;
C_est=0.5*(lb+ub);
C(isnan(C))=C_est(isnan(C));
lb_lnqmu=-lb_lnqmu+mu_kappa.*C;

lb_lb_mu=lnpmu-lb_lnqmu;
lb_lb_mu=sum(lb_lb_mu(occ_clusters));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% kappa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lnpkappa=(1e-9-1).*Elnkappa-1e-9*Ekappa;
% lnqkappa=kappa_a.*log(kappa_b)-gammaln(kappa_a)+(kappa_a-1).*Elnkappa-kappa_b.*Ekappa;
lb_kappa=lnpkappa-lnqkappa;
lb_kappa=sum(lb_kappa(occ_clusters));

%%%%%%%%%%%%%%%%%%%%%%%%%%
% z_n
%%%%%%%%%%%%%%%%%%%%%%%%
lnqz=phi_z.*log(phi_z);
lnqz=-sum(lnqz(phi_z>0));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  p(x_n|z_n)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lnpx=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),c_kappa').*phi_z;
lnpx=sum(lnpx(:));
% lb_z=-sum(lb_z);

L=lnpx+lb_alpha+lb_V+lb_lb_mu+lb_kappa+lnqz;
% lik=[L,lnpx,lb_alpha,lb_V,lb_lb_mu,lb_kappa,lnqz];