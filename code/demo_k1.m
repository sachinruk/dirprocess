clear all
close all
% clc

load k1.mat

% w=tfidf2(x')';
w=k1;
w2=tfidf(w);
w2 = convert_to_unit(w2);

%completely unsupervised
rng(1);
[phi_z_best,class_best,Eeta1_best,L_best]=EM(w2);
disp([nmi3(class_best,label_num), RandIndex(class_best,label_num)]);

%train on half
rng(1);
training=floor(length(label_num)/2);
labels_train=label_num(1:training);
[phi_z2,class2,Eeta2,L_best2]=EM_semiSupervised(w2,labels_train,1);
disp([nmi3(class2((training+1):end),label_num((training+1):end)),...
    RandIndex(class2((training+1):end),label_num((training+1):end))]);

%k-means 
rng(1);
[~,idx]=k_means_normal(w,30,400,1);
disp([nmi3(label_num,idx) RandIndex(label_num,idx)])

rng(1);
idx2=kmeans(w2,length(unique(class_best)),'Distance','cosine');
disp([nmi3(label_num,idx2) RandIndex(label_num,idx2)])

rng(1);
idx3=kmeans(w2,length(unique(label_num)),'Distance','cosine');
disp([nmi3(label_num,idx3) RandIndex(label_num,idx3)])

rng(1);
idx4=kmeans(w2,5,'Distance','cosine');
disp([nmi3(label_num,idx4) RandIndex(label_num,idx4)])