function [L, d_params, H]=lb_eta2(params,C1,C2)
if length(params)>2
    disp('uh oh');
end
lna=params(1); lnb=params(2);
lna_b=lna-lnb; lna_2b=lna_b-lnb;
a=exp(lna); 
% b=exp(lnb);
% set lower limits to parameters
if a<1e-10, a=1e-10; end 
% if b<1e-10, b=1e-10; end
% L=-C1*a.*(a+1)./(b.^2)+C2*a./b+gammaln(a)-a.*psi(a)+a;
L=-C1*exp(lna_2b).*(a+1)+C2*exp(lna_b)+gammaln(a)-a.*psi(a)+a;

L=-L;
%derivatives (jacobian)
% L_a=-C1*(2*a+1)./(b.^2)+C2./b-a.*psi(1,a)+1;
% L_b=2*C1*a.*(a+1)./(b.^3)-C2*a./(b.^2);
dL_dlna=-C1*(2*a+1).*exp(lna_2b)+C2*exp(lna_b)-a.^2.*psi(1,a)+a;
dL_dlnb=2*C1*exp(lna_2b).*(a+1)-C2*exp(lna_b);

d_params=-[dL_dlna; dL_dlnb];
%Hessian
% L_aa=-2*C1./b.^2-psi(1,a)-a.*psi(2,a);
% L_bb=-6*C1*a.*(a+1)./(b.^4)+2*C2*a./(b.^3);
% L_ab=2*C1*(2*a+1)./(b.^3)-C2./(b.^2);

% Hessian wrt to logs
d2L_dlna2=-2*C1*exp(2*lna_b)-psi(1,a).*a.^2-a.^3.*psi(2,a)+dL_dlna;
d2L_dlnb2=-6*C1*(a+1).*exp(lna_2b)+2*C2*exp(lna_b)+dL_dlnb;
d2L_dlnadlnb=2*C1*(2*a+1).*exp(lna_2b)-C2.*exp(lna_b);

H=-[d2L_dlna2 d2L_dlnadlnb;
    d2L_dlnadlnb d2L_dlnb2];