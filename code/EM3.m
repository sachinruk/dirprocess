function [phi_z_best,class_best,Emu_best,L_best]=EM3(x)
T=100; N=size(x,1);

L_best=-inf;
restarts=40;
for iter=1:restarts
    phi_z=ones(N,T)/T;
    Ealpha=10*rand;
%     Ekappa=0.5+0.5*rand(T,1); 
    Emu=k_means(x,100,20,1);
    L=zeros(15,1);
    for i=1:100
        [Ekappa, Elnkappa,c_kappa,Elnq]=q_kappa(x,phi_z,Emu);    
        [Emu,mu_kappa]=q_mu(x,phi_z,Ekappa);
        [lnV, ln1_V,V_a,V_b]=qV(phi_z,Ealpha);
        [Ealpha, a,b]=q_alpha(ln1_V);     
        phi_z=qz(x,lnV,ln1_V,Emu,Ekappa,c_kappa);
        L(i)=lnlb3(x,a,b,Ealpha,V_a,V_b,lnV,ln1_V,mu_kappa,Emu,...
                                    Ekappa,Elnkappa,c_kappa,Elnq,phi_z);
        if i>1
            if i>15 && abs(L(i)-L(i-1))<1e-9
                break;
            end
        end
    end
    [~,class]=max(phi_z,[],2);
    subplot(8,5,iter);plot(L); xlabel(length(unique(class)));
    if L(end)>L_best
        L_best=L(end);
        phi_z_best=phi_z;
%         class_best=class;
        Emu_best=Emu;
    end    
end

[~,class_best]=max(phi_z_best,[],2);
Emu_best=Emu_best(:,unique(class_best,'stable'));
class_best=relabel(class_best);
Emu_best=bsxfun(@rdivide,Emu_best,sqrt(sum(Emu_best.^2)));

%gamma distribution approximation
function [Ealpha, a,b]=q_alpha(ln1_V)
a=length(ln1_V)+1e-6;
b=1e-6-sum(ln1_V);
Ealpha=a/b;
% Elnalpha=psi(a)-log(b);

%beta distribution approximation
function [lnV, ln1_V, gamma_1,gamma_2]=qV(phi_z,Ealpha)
gamma_1=(sum(phi_z)+1)';
phi_z=cumsum(phi_z,2);
phi_z=bsxfun(@minus,phi_z(:,end),phi_z);
gamma_2=(sum(phi_z)+Ealpha)';
% gamma=[gamma_1 gamma_2];

%expectations
lnV=psi(gamma_1)-psi(gamma_1+gamma_2);
ln1_V=psi(gamma_2)-psi(gamma_1+gamma_2);

%multinomail approxmiation
function p=qz(x,lnV,ln1_V,Emu,Ekappa,c_kappa)
d=size(x,2);
ln1_V=cumsum(ln1_V);
ln1_V=[0; ln1_V(1:(end-1))];

% C1=sum(p)'/(2*(d-2)); 
% C2=x*Emu;
% k=C2.^2./(4*C1);


% lnVMF=bsxfun(@plus,C2,Ekappa'),-Ekappasq'/(2*(d-2)));
lnVMF=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),c_kappa');

p=bsxfun(@plus,lnVMF,(ln1_V+lnV)');
p=exp(bsxfun(@minus,p,max(p,[],2)));
p=bsxfun(@rdivide, p,sum(p,2));

%vMF approxmiation
function [Emu,mu_kappa]=q_mu(x,p,Ekappa)
%prior parameters
kappa=1e-6; d=size(x,2); 
% T=length(Ekappa); 
mu=1./sqrt(d);
prior_const=kappa*mu;
% mu=zeros(d,T); mu_kappa=zeros(T,1);
% for t=1:T
%     v=sum(bsxfun(@times,x,p(:,t)*Ekappa(t)))'+prior_const;
%     mu(:,t)=v/norm(v);
%     mu_kappa(t)=norm(v);
% end
v=bsxfun(@times,x'*p,Ekappa')+prior_const;
mu_kappa=sqrt(sum(v.^2));
mu=bsxfun(@rdivide,v,mu_kappa);
% Emu=v/(d-2);
C=besseli(d/2,mu_kappa)./besseli(d/2-1,mu_kappa);

lb=sqrt(1+((d/2)./mu_kappa).^2)-(d/2)./mu_kappa; 
ub=sqrt(1+((d/2-0.5)./mu_kappa).^2)-(d/2-0.5)./mu_kappa;
C_est=0.5*(lb+ub);
C(isnan(C))=C_est(isnan(C));

Emu=bsxfun(@times,mu,C)';

%gamma approximation on kappa
function [Ekappa, Elnkappa,c_kappa,Elnq]=q_kappa(x,p,mu)
[N,d]=size(x); T=size(p,2);
%parameters of gamma distribution(s)
C=(d-1)/2;
C1=C*sum(p)'./2;
a=2*C1+1e-9;
b=sum((1-x*mu').*p)'+1e-9;

%instance where two gamma distributions are used
idx=C1>1;
%preallocate memory
Ekappa=zeros(T,1);
Elnkappa=zeros(T,1); 
Elnq=zeros(T,1);
c_kappa=zeros(T,1);
if sum(idx)
    a1=a(idx)'; a2=1e-9; C1_idx=C1(idx)'; b_idx=b(idx)';
    lnw1=gammaln(a1)-(a1).*log(b_idx);
    lnw2=C1_idx.*log(2)+(2*C1_idx)*log(C)+gammaln(a2)-a2*log(b_idx);
    logNC= logsumexp([lnw1; lnw2]);
    w=exp(bsxfun(@minus,[lnw1' lnw2'],logNC'));
    Ekappa(idx)=logsumexp([gammaln(a1+1)-(a1+1).*log(b_idx);...
        C1_idx.*log(2)+(2*C1_idx)*log(C)+gammaln(1+a2)-(1+a2)*log(b_idx)]);
    Ekappa(idx)=exp(Ekappa(idx)'-logNC);
    
%     kappa=gamma_samples(w(:,1),a1,a2,b_idx,10000);
    c_kappa(idx)= lb_c(C,a1',a2,b_idx',w(:,1));
    psi_a1=psi(a1);
    psi_a2=psi(1e-9);
    
    Elnkappa(idx)=psi_a1'.*w(:,1)+psi_a2.*w(:,2);
    
    lnint= mix_lkappa(C,C1_idx',a1',a2,b_idx',w(:,1));
    Elnq(idx)=(1e-9-1)*Elnkappa(idx)+lnint-logNC';
end
idx2=(a<1e-8 & b<1e-8) & ~idx;
idx3=~(a<1e-8 & b<1e-8) & ~idx;
c_kappa(idx2)=-1e9;
c_kappa(idx3)= lb_c(C,a(idx3),0,b(idx3),1); 
Ekappa(~idx)=a(~idx)./b(~idx);
Elnkappa(~idx)=psi(a(~idx));
Elnkappa=Elnkappa-log(b);
Elnq(~idx)=(a(~idx)-1).*Elnkappa(~idx)+a(~idx).*log(b(~idx))-gammaln(a(~idx));
Elnq=Elnq-Ekappa.*b;

% function val=lb_c(kappa,a,b,C,C1,logNC)
% if (logNC)
%     q_kappa=-kappa.*b+logsumexp([(a-1)*log(kappa); ...
%                         C1.*log(2)+(2*C1)*log(C)+(1e-9-1)*log(kappa)])-logNC;
% else
%     q_kappa=-kappa.*b+(a-1)*log(kappa)+a*log(b)-gammaln(a);
% end
% lb=-sqrt(kappa.^2+C^2)+C*log(sqrt(kappa.^2+C^2)+C);
% val=lb*exp(q_kappa);

% function val=mix_kappa(kappa,a,b,C,C1,logNC)
% mix=logsumexp([(2*C1).*log(kappa); C1.*log(2)+(2*C1)*log(C)]);
% q_kappa=-kappa.*b+logsumexp([(a-1)*log(kappa); ...
%                         C1.*log(2)+(2*C1)*log(C)+(1e-9-1)*log(kappa)])-logNC;
% val=mix*exp(q_kappa);

% function val=mix_kappa(kappa,C,C1)
% w1=bsxfun(@times,log(kappa),2*C1);
% w2=C1.*log(2)+(2*C1)*log(C);
% mix=logsumexp([w1; w2]);
% val=mean(mix);
% 
% function val=lb_c(kappa,C)
% lb=-sqrt(kappa.^2+C^2)+C*log(sqrt(kappa.^2+C^2)+C);
% val=mean(lb);

% function kappa=gamma_samples(w,a1,a2,b,n)
% 
% T=length(b);
% b=1./b; %different notation to how gamma is used in matlab
% kappa=zeros(n,T);
% for i=1:T
%     r1=gamrnd(a1(i),b(i),n,1);
%     r2=gamrnd(a2,b(i),n,1);
%     z=w(i)>rand(n,1);
%     kappa(:,i)=r1.*z+r2.*(1-z);
% end