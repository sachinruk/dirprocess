function [phi_z_best,class_best,Eeta1_best,L_best]=EM2(x)
T=100; N=size(x,1);

L_best=-inf;
restarts=40;
for iter=1:restarts
    phi_z=ones(N,T)/T;
    Ealpha=10*rand;
    Eeta2=0.5+0.5*rand(T,1); 
    L=zeros(15,1);
    for i=1:100
        [Eeta1,eta_kappa]=q_eta1(x,phi_z,Eeta2);
        [Eeta2, Elneta2,eta2_a,eta2_b]=q_eta2(x,phi_z,Eeta1);    
        [lnV, ln1_V,V_a,V_b]=qV(phi_z,Ealpha);
        [Ealpha, a,b]=q_alpha(ln1_V);     
        phi_z=qz(x,lnV,ln1_V,Eeta1,Eeta2,Elneta2);
        L(i)=lnlb(x,a,b,Ealpha,V_a,V_b,lnV,ln1_V,eta_kappa,Eeta1,eta2_a,eta2_b,Eeta2,Elneta2,phi_z);
        if i>1
            if i>15 && abs(L(i)-L(i-1))<1e-9
                break;
            end
        end
    end
%     [~,class]=max(phi_z,[],2);
%     subplot(8,5,iter);plot(L); xlabel(length(unique(class)));
    if L(end)>L_best
        L_best=L(end);
        phi_z_best=phi_z;
        Eeta1_best=Eeta1;
    end    
end

[~,class_best]=max(phi_z_best,[],2);
Eeta1_best=Eeta1_best(:,unique(class_best,'stable'));
class_best=relabel(class_best);
Eeta1_best=bsxfun(@rdivide,Eeta1_best,sqrt(sum(Eeta1_best.^2)));

%gamma distribution approximation
function [Ealpha, a,b]=q_alpha(ln1_V)
a=length(ln1_V)+1e-6;
b=1e-6-sum(ln1_V);
Ealpha=a/b;
% Elnalpha=psi(a)-log(b);

%beta distribution approximation
function [lnV, ln1_V, gamma_1,gamma_2]=qV(phi_z,Ealpha)
gamma_1=(sum(phi_z)+1)';
phi_z=cumsum(phi_z,2);
phi_z=bsxfun(@minus,phi_z(:,end),phi_z);
gamma_2=(sum(phi_z)+Ealpha)';
% gamma=[gamma_1 gamma_2];

%expectations
lnV=psi(gamma_1)-psi(gamma_1+gamma_2);
ln1_V=psi(gamma_2)-psi(gamma_1+gamma_2);

%multinomail approxmiation
function p=qz(x,lnV,ln1_V,Eeta1,Eeta2,Elneta2)
d=size(x,2);
ln1_V=cumsum(ln1_V);
ln1_V=[0; ln1_V(1:(end-1))];

lnVMF=bsxfun(@plus,bsxfun(@times,x*Eeta1,Eeta2'),(0.5*(d-1)*Elneta2-Eeta2)');

p=bsxfun(@plus,lnVMF,(ln1_V+lnV)');
p=exp(bsxfun(@minus,p,max(p,[],2)));
p=bsxfun(@rdivide, p,sum(p,2));

%vMF approxmiation
function [Eeta1,eta_kappa]=q_eta1(x,p,Eeta2)
%prior parameters
kappa=1e-6; d=size(x,2); 
% T=length(Eeta2); 
mu=1./sqrt(d);
prior_const=kappa*mu;

v=bsxfun(@times,x'*p,Eeta2')+prior_const;
eta_kappa=sqrt(sum(v.^2));
eta_mu=bsxfun(@rdivide,v,eta_kappa);
% Eeta1=v/(d-2);
C=besseli(d/2,eta_kappa)./besseli(d/2-1,eta_kappa);

lb=sqrt(1+((d/2)./eta_kappa).^2)-(d/2)./eta_kappa; 
ub=sqrt(1+((d/2-0.5)./eta_kappa).^2)-(d/2-0.5)./eta_kappa;
C_est=0.5*(lb+ub);
C(isnan(C))=C_est(isnan(C));

Eeta1=bsxfun(@times,eta_mu,C);

%gamma approximation on eta2
function kappa=kappa_ml(x,p,eta_mu)
d=size(x,2);
C1=(d-1)/2;
C2=sum((x*eta_mu).*p);
delta=((C1-1)*C2).^2-(C2-1)*(1-2*C1);
kappa=(-(C1-1)*C2+sqrt(delta))./(C2-1);
kappa(kappa<0)=0;