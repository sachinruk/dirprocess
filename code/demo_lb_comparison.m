clear all
close all
clc

x=1:1:200;
d=5; 
nu=d/2-1;
y1=nu*log(x)-log(besseli(nu,x));
lb=0.5*(d-1)*log(x)-x;
figure;
plot(x,y1); hold on; plot(x,lb,'r');

d=50; 
nu=d/2-1;
y1=nu*log(x)-log(besseli(nu,x));
lb=0.5*(d-1)*log(x)-x;
figure;
plot(x,y1); hold on; plot(x,lb,'r');

d=500; 
nu=d/2-1;
y1=nu*log(x)-log(besseli(nu,x));
lb=0.5*(d-1)*log(x)-x;
idx=(y1==inf);
y1(idx)=nu*log(x(idx))-nu*log(0.5*x(idx))+gammaln(nu+1);
figure;
plot(x,y1); hold on; plot(x,lb,'r');