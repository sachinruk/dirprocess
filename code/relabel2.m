function [class, ordered_eta]=relabel2(class,Eeta)

u_classes=unique(class,'stable');
max_class=max(u_classes);
ordered_eta=zeros(length(u_classes),size(Eeta,2));
for i=1:length(u_classes)
    class(class==u_classes(i))=max_class+i;
    v=Eeta(u_classes(i),:);
    ordered_eta(i,:)=v./norm(v);
end
class=class-max_class;