clear all
close all
clc

load nips_1-17.mat

%clear up vocabulary words that are not used
x=counts';   
% w=tfidf2(x')';
w=tfidf(x);
w2 = convert_to_unit(w);

rng(1);
[phi_z,class,Eeta,llb]=EM(w2);

cl=unique(class);
mu=Eeta(cl,:);

[mu_sort,idx] = sort(mu,2,'descend');
pop_words=cell(size(mu(:,1:10)));
k=1;
for i=1:size(mu,1)
    %pick out top 10 words
    for j=1:10
        pop_words{i,j}=words{idx(i,j)};
        k=k+1;
    end
end

rng(1);
[phi_z2,class2,Eeta2,llb2]=EM_nested(w2,5);
cl2=unique(class2{7});
mu2=Eeta2{6}(cl2,:);

[mu_sort2,idx2] = sort(mu2,2,'descend');
pop_words2=cell(size(mu2(:,1:10)));
for i=1:size(mu2,1)
    %pick out top 10 words
    for j=1:10
        pop_words2{i,j}=words{idx2(i,j)};
    end
end

[mu_sort3,idx3]=sort(Eeta2{2}(1,:),'descend');
pop_words3=cell(1,10);
for j=1:10
    pop_words3{j}=words{idx3(j)};
end