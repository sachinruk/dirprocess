% 1 class = 1 node
% rng('default');
close all;clear all; clc;

maxDepth = 3;

%load data
features = load('allSyntheticData.txt');
[ nData,nDim ] = size(features);
%unit
ufeatures = convert_to_unit(features);
%final features
x = ufeatures;

docClass_level = []; %col is level, starts from level = maxDepth
ixlevel = 1;

level_class = {};
level = maxDepth;

% rng(1);
% [phi_z,class,eta_mu]=EM(x);
rng(1);
[phi_z,class,Eeta]=EM_nested(x,3);