function [L, d_a]=test_func2(a,C1,C2)

k=C2^2/(4*C1);
L=k.*a./(a+1)+gammaln(a)-(a-1e-6).*psi(a)+a+1e-6*log(a+1);

L=-L;
%derivatives (jacobian)
L_a=k./(a+1).^2-(a-1e-6).*psi(1,a)+1+1e-6./(a+1);
d_a=-L_a;
% L_b=2*C1*a.*(a+1)./(b.^3)-C2*a./(b.^2);

% d_params=-[L_a; L_b];