clear all
close all
clc

load faces.mat

w = convert_to_unit(pix);
idx=sqrt(sum(pix.^2,2))==0;
emotion(idx)=[];
emotion=relabel(emotion);

%(semi) supervised learning part
rng(1);
[phi_z_s,class_s,Eeta1_s]=EM_semiSupervised(w,emotion(1:10000));
figure; k=1;
for i=1:59
     subplot(5,12,k); imshow(reshape(Eeta1_s(:,i),48,48)',[]);
     k=k+1;
end
nmi3(class_s,emotion)
RandIndex(class_s,emotion)


%unsupervised learning
[phi_z,class,Eeta,llb]=EM_nested(w,2);
nmi3(class{4},emotion)
RandIndex(class{4},emotion)
C=confusion_matrix(class{4},emotion);
u=unique(class{4});
eta=Eeta{3}(u,:);
figure; k=1;
for i=1:12
     subplot(3,5,k); imshow(reshape(eta(i,:),48,48)',[]);
     k=k+1;
end

%unsupervised learning with 3 level hierarchy
rng(1);
[phi_z2,class2,Eeta2,llb2,class_hierarchy,x2]=EM_nested(w,3);

%(semi) supervised hierarchical learning
rng(1);
[phi_z4,class4,Eeta4,llb4,class_hierarchy4]=EM_ss_nested(w,emotion(1:10000),3);
u=unique(class4{5});
eta=Eeta4{4}(u,:);
figure; k=1;
for i=1:11
     subplot(3,4,k); imshow(reshape(eta(i,:),48,48)',[]);
     k=k+1;
end
u=unique(class4{4});
eta=Eeta4{3}(u,:);
figure; k=1;
for i=1:2
     subplot(3,4,k); imshow(reshape(eta(i,:),48,48)',[]);
     k=k+1;
end
u=unique(class4{3});
eta=Eeta4{2}(u,:);
figure; k=1;
for i=1:1
     subplot(3,4,k); imshow(reshape(eta(i,:),48,48)',[]);
     k=k+1;
end

C2=confusion_matrix(class4{5},emotion);