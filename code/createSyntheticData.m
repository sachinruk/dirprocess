function [ X, totalDatapoints ] = createSyntheticData(numbPointsPerCluster, noiseRatio, numbClusters, numbFeaturesPerCluster)
% This function will create synthetic data (in unit vectors)
% Each cluster will use (dim/numbClusters) as its own features,
%    for example: dim = 10, numbClusters = 2, then the 1st five features
%                 are assigned to cluster 1, and the 2nd to cluster 2
%
% Input:  noiseRat = [0,1]
% Output: X has a structure:
%           X{c}.data = numbPointsPerCluster x dim
%           X{c}.mu = 1 x dim
%           X{c}.kappa = 1 x 1
%           X{c}.dataInt = numbPointsPerCluster x dim -> contains int val
%         where c is the cluster
%
% Author: Didi Surian
% Ver. 1.0

% rng('default');

% To be used to generate random number
IMIN = 10;
IMAX = 20;

totalDimension = (numbFeaturesPerCluster * numbClusters);
% totalDimension=numbFeaturesPerCluster;
totalDatapoints = (numbPointsPerCluster * numbClusters);
fprintf('Creating %d data points with dimension %d\n',totalDatapoints,totalDimension);

lw = zeros(1,totalDimension);
vk = 1;

for c=1:numbClusters
    for v=1:numbFeaturesPerCluster
        lw(vk) = c;
        vk = vk + 1;
    end
    ix = find(lw == c);  
    
    for n=1:numbPointsPerCluster
        dataPoints = zeros(1,totalDimension);
        dataPoints(1,ix) = randi([IMIN,IMAX],1,numbFeaturesPerCluster);
        
        %inject noise
        ixzeros = find(dataPoints == 0);               %find index where value = 0
        noisePortion = ceil(length(ixzeros) * noiseRatio); %calc noise portion
        ixinject = randperm(length(ixzeros),noisePortion); %take portion = noise portion
        noises = randi([1,5],1,length(ixinject));          %create noise
        dataPoints(1,ixzeros(ixinject)) = noises;           %inject noise        
        X{c}.dataInt(n,:) = dataPoints;
        
        %convert to unit
        dataPoints= convert_to_unit(dataPoints); 
        
        X{c}.data(n,:) = dataPoints;                
    end;
    
    %compute mu
    r = sum(X{c}.data,1);    
    X{c}.mu = r/norm(r);
    
    %compute approximate kappa
    rbar = norm(r)/size(X{c}.data,1);
    d = totalDimension;
    X{c}.kappa = (rbar*d - rbar^3)/(1-rbar^2);    
end
