close all
clear all
clc

rng(1);
figure; hold on;
plotSphere();
kappa=50; n=50;
true_directions=[[1; 1; 1] -[1; 1; 1] [-0.5; 4; 1] [-2; 0.5; -1]];
x=randvonMisesFisherm(3, n, kappa, true_directions(:,1))';
% plot(x(:,1),x(:,2),'.')

x2=randvonMisesFisherm(3, n, kappa, true_directions(:,2))';
% plot(x2(:,1),x2(:,2),'r.')

x3=randvonMisesFisherm(3, n, kappa, true_directions(:,3))';
% plot(x3(:,1),x3(:,2),'g.')

x4=randvonMisesFisherm(3, n, kappa, true_directions(:,4))';
% plot(x4(:,1),x4(:,2),'k.')



x=[x; x2; x3; x4];
figure; plot3(x(:,1),x(:,2),x(:,3),'.'); plotSphere();
view(45,45);

[phi_z_best,class_best,Eeta1_best]=EM(x);
figure; hold on;
plotSphere();
u=unique(class_best);
c=jet(length(u));
for i=1:length(u)
    plot3(x(class_best==u(i),1),x(class_best==u(i),2),x(class_best==u(i),3),'.','color',c(i,:));
end
view(135,45);

%normalise
len=sqrt(sum(true_directions.^2));
true_directions=bsxfun(@rdivide,true_directions,len);
%dot product with inferred directions
dot_prod=diag(true_directions'*Eeta1_best');
disp(dot_prod');