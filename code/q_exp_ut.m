function [factor,u]=q_exp_ut(init_u,b,p,C)
T=length(b);
CCt=C*sum(p)';
idx=CCt==0;
% init_u=(CCt./b).^2-C^2;
% init_u(init_u>0)=sqrt(init_u(init_u>0));
% init_u(init_u<0)=1e-9;
u=zeros(T,1);
u(idx)=10;
change_idx=find(~idx);
opt=optimset('GradObj','on','TolFun',1e-3,'TolX',1e-3,'Display','off');
for t=1:length(change_idx)
    u(change_idx(t))=fminunc(@(u)L2(u,CCt(change_idx(t)),C,b(change_idx(t))),init_u(change_idx(t)),opt);
end

factor=1-1./sqrt(exp(u)+1);

function [f_sig,g_sig]=L2(u,CCt,C,b)
sub_fn=1-1./sqrt(exp(u)+1);
sub_fn2=log(1+sqrt(1+exp(u)));
extreme_idx=u>500;
sub_fn2(extreme_idx)=u(extreme_idx)/2;
f=CCt*(sub_fn2-sub_fn.*(log(C)+log(b)+0.5*u))...
    +gammaln(CCt.*sub_fn+1e-9);
% f=-f;
k=CCt*exp(u)./(2*(exp(u)+1).^1.5);
k(extreme_idx)=exp(log(CCt)+u(extreme_idx)-1.5*u(extreme_idx)-log(2));
g=k.*(-log(C)-0.5*u-log(b)+psi(CCt*(sub_fn)+1e-9));
% g=-g;

%convert to sigmoid function
f_sig=(1./(1+exp(-f)));
g_sig=f_sig.*(1-f_sig).*g;
f_sig=-f_sig;
g_sig=-g_sig;