clear all
close all
clc

I=double(imread('lena_gray_256.tif'));
%break up into block_len*block_len blocks
block_len=8;
features=zeros(256^2/block_len^2,block_len^2+2);
row=1;col=1;
for i=1:size(features,1)
    I_block=I(row:row+(block_len-1),col:col+(block_len-1));
    features(i,:)=[I_block(:)', row, col];
    row=row+block_len;
    if row>256
        col=col+block_len;
        row=1;
    end
end

% mean_f=mean(features);
% features=bsxfun(@minus,features,mean_f);
feature_length=sqrt(sum(features.^2,2));
features=bsxfun(@rdivide,features,feature_length);

[phi_z,class,eta_mu]=EM(features);

%paint the classes
class_pic=zeros(size(I));
row=1; col=1;
for i=1:length(class)
    class_pic(row:row+(block_len-1),col:col+(block_len-1))=class(i);
    row=row+block_len;
    if row>256
        col=col+block_len;
        row=1;
    end
end
cmap = jet(256);
figure; subplot(121); imshow(I,[]);
subplot(122); imshow(class_pic,[],'colormap',cmap);