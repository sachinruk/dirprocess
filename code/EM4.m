function [phi_z_best,class_best,Emu_best,L_best]=EM4(x)
T=100; [N,d]=size(x);

L_best=-inf;
restarts=80;
C=(d-1)/2;
for iter=1:restarts
    phi_z=ones(N,T)/T;
    Emu=k_means(x,100,20,1);
    Ealpha=20+(T-20)*rand;
    L=zeros(15,1);
    factor=ones(T,1);
    u=zeros(T,1)+50;
    for i=1:100
        [Ekappa, Elnkappa,kappa_a,kappa_b]=q_kappa(x,phi_z,Emu,C,factor);
%         [factor, u]=q_exp_ut(u,kappa_b,phi_z,C);
        
        [factor,u]=q_exp_ut2(Elnkappa,C);
        [Emu,mu_kappa]=q_mu(x,phi_z,Ekappa);
        [lnV, ln1_V,V_a,V_b]=qV(phi_z,Ealpha);
        [Ealpha, a,b]=q_alpha(ln1_V);     
        phi_z=qz(x,lnV,ln1_V,Emu,Ekappa,Elnkappa,u,C);
        L(i)=lnlb4(x,a,b,Ealpha,V_a,V_b,lnV,ln1_V,mu_kappa,Emu,kappa_a,kappa_b,Ekappa,Elnkappa,u,phi_z);
        if i>1
            if (i>15 && abs(L(i)-L(i-1))<1e-5) || (i>15 && abs(L(i)-L(i-1))/L(i-1)<1e-7)
                break;
            end
        end
    end
%     [~,class]=max(phi_z,[],2);
%     subplot(8,10,iter);plot(L); xlabel(length(unique(class)));
    if L(end)>L_best
        L_best=L(end);
        phi_z_best=phi_z;
%         class_best=class;
        Emu_best=Emu;
    end    
end

[~,class_best]=max(phi_z_best,[],2);
Emu_best=Emu_best(:,unique(class_best,'stable'));
class_best=relabel(class_best);
Emu_best=bsxfun(@rdivide,Emu_best,sqrt(sum(Emu_best.^2)));

%gamma distribution approximation
function [Ealpha, a,b]=q_alpha(ln1_V)
a=length(ln1_V)+1e-6;
b=1e-6-sum(ln1_V);
Ealpha=a/b;
% Elnalpha=psi(a)-log(b);

%beta distribution approximation
function [lnV, ln1_V, gamma_1,gamma_2]=qV(phi_z,Ealpha)
gamma_1=(sum(phi_z)+1)';
phi_z=cumsum(phi_z,2);
phi_z=bsxfun(@minus,phi_z(:,end),phi_z);
gamma_2=(sum(phi_z)+Ealpha)';
% gamma=[gamma_1 gamma_2];

%expectations
lnV=psi(gamma_1)-psi(gamma_1+gamma_2);
ln1_V=psi(gamma_2)-psi(gamma_1+gamma_2);

%multinomail approxmiation
function p=qz(x,lnV,ln1_V,Emu,Ekappa,Elnkappa,u,C)
d=size(x,2);
ln1_V=cumsum(ln1_V);
ln1_V=[0; ln1_V(1:(end-1))];

% C1=sum(p)'/(2*(d-2)); 
% C2=x*Emu;
% k=C2.^2./(4*C1);

ln_exp_u_term=log(1+sqrt(1+exp(u)));
extreme_idx=u>500;
ln_exp_u_term(extreme_idx)=u(extreme_idx)/2;
c_kappa=-Ekappa+C*ln_exp_u_term+...
    0.5*C*(1-1./(sqrt(exp(u)+1))).*(2*Elnkappa-2*log(C)-u);


% lnVMF=bsxfun(@plus,C2,Ekappa'),-Ekappasq'/(2*(d-2)));
lnVMF=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),c_kappa');

p=bsxfun(@plus,lnVMF,(ln1_V+lnV)');
p=exp(bsxfun(@minus,p,max(p,[],2)));
p=bsxfun(@rdivide, p,sum(p,2));

%vMF approxmiation
function [Emu,mu_kappa]=q_mu(x,p,Ekappa)
%prior parameters
kappa=1e-6; d=size(x,2); 
% T=length(Ekappa); 
mu=1./sqrt(d);
prior_const=kappa*mu;
% mu=zeros(d,T); mu_kappa=zeros(T,1);
% for t=1:T
%     v=sum(bsxfun(@times,x,p(:,t)*Ekappa(t)))'+prior_const;
%     mu(:,t)=v/norm(v);
%     mu_kappa(t)=norm(v);
% end
v=bsxfun(@times,x'*p,Ekappa')+prior_const;
mu_kappa=sqrt(sum(v.^2));
mu=bsxfun(@rdivide,v,mu_kappa);
% Emu=v/(d-2);
C=besseli(d/2,mu_kappa)./besseli(d/2-1,mu_kappa);

lb=sqrt(1+((d/2)./mu_kappa).^2)-(d/2)./mu_kappa; 
ub=sqrt(1+((d/2-0.5)./mu_kappa).^2)-(d/2-0.5)./mu_kappa;
C_est=0.5*(lb+ub);
C(isnan(C))=C_est(isnan(C));

Emu=bsxfun(@times,mu,C)';

%gamma approximation on kappa
function [Ekappa, Elnkappa,a,b]=q_kappa(x,p,mu,C,factor)
a=C*sum(p)'; 
a=a.*factor+1e-9;
b=sum((1-x*mu').*p)'+1e-9; % C2=sum((p'*x)'.*mu)';<- can be used instead
Ekappa=a./b;
Elnkappa=psi(a)-log(b);

function [factor,u]=q_exp_ut2(Elnkappa,C)
u=2*(Elnkappa-log(C));

factor=1-1./sqrt(exp(u)+1);
extreme_idx=u>500;
factor(extreme_idx)=1;