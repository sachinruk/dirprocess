clear all
close all
clc

load activity.mat

w=convert_to_unit(Xtrain);
labels=ytrain;
% labels=labels(2:end);
train_set=500;
%unsupervised vMF
rng(1);
[phi_z,class,Eeta,llb]=EM(w);
switches1=length(class)-sum(diff(class)==0);

disp([nmi3(class((train_set+1):end),labels((train_set+1):end)) RandIndex(class((train_set+1):end),labels((train_set+1):end))]);
C1=confusion_matrix(class,labels);

%semi-supervised vMF
rng(1);
[phi_z4,class2,Eeta4,llb4]=EM_semiSupervised(w,labels(1:train_set),0);
disp([nmi3(class2((train_set+1):end),labels((train_set+1):end)) RandIndex(class2((train_set+1):end),labels((train_set+1):end))]);
C2=confusion_matrix(class2((train_set+1):end),labels((train_set+1):end));

%unsupervised with time consistency
rng(1);
p=0.9;
[phi_z2,Ew2,class3,class3_t,Eeta2]=EM_t(w,p);
switches2=length(class3_t)-(sum(diff(class3_t)==0));

disp([nmi3(class3_t,labels) RandIndex(class3_t,labels)]);

%semisupervised with time consistency
rng(1);
lnp=log([p 1-p]);
[phi_z3,Ew3,class4,class4_t,Eeta3]=EM_t_ss(w,lnp,labels(1:train_set));
switches3=length(class4_t)-(sum(diff(class4_t)==0));

disp([nmi3(class4_t((train_set+1):end),labels((train_set+1):end)) RandIndex(class4_t((train_set+1):end),labels((train_set+1):end))]);

p=0.958;
rng(1);
lnp=log([p 1-p]);
[phi_z5,Ew5,class5,class5_t,Eeta5]=EM_t_ss(w,lnp,labels(1:train_set));
switches5=length(class5_t)-(sum(diff(class5_t)==0));

disp([nmi3(class5_t((train_set+1):end),labels((train_set+1):end)) RandIndex(class5_t((train_set+1):end),labels((train_set+1):end))]);

%look at the accuracies of the two models
acc_normal=sum(class2((train_set+1):end)==labels((train_set+1):end))/length(labels((train_set+1):end));
acc_t=sum(class4_t((train_set+1):end)==labels((train_set+1):end))/length(labels((train_set+1):end));
acc_t2=sum(class5_t((train_set+1):end)==labels((train_set+1):end))/length(labels((train_set+1):end));
%k-means 
idx=kmeans(Xtrain,6);
disp([nmi3(labels,idx) RandIndex(labels,idx)]);

idx2=kmeans(w,6,'Distance','cosine');
disp([nmi3(labels,idx2) RandIndex(labels,idx2)]);

disp([length(unique(class)) length(unique(class3_t))...
       length(unique(class2)) length(unique(class4_t))]);