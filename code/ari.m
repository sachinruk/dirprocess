function ari_=ari(l,c)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Inputs
% l = (true) labels
% c = (assigned) clusters

%Create confusion matrix
c=relabel(c);
l=relabel(l);
confusion_matrix=zeros(max(l),max(c));

for i=1:max(l)
    for j=1:max(c)
        confusion_matrix(i,j)=sum(c(l==i)==j);
    end
end

N=length(l);
l_sum=sum(confusion_matrix,2);
c_sum=sum(confusion_matrix);

for i=1:
nchoose2=nchoosek(confusion_matrix(:),2);
lchoose2=sum(nchoosek(l_sum,2));
cchoose2=sum(nchoosek(c_sum,2));

ari_=(sum(nchoose2(:))-lchoose2*cchoose2/nchoosek(N,2))/...
    (0.5*(lchoose2+cchoose2)-lchoose2*cchoose2/nchoosek(N,2));
