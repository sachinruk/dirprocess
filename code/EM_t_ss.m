function [phi_z_best,Ew_best,class_best,class2,Emu_best]=EM_t_ss(x,lnp_sticky,labels)
T=100; [N,d]=size(x);
train_set=length(labels);
phi_z_train=zeros(train_set,T);
phi_z_train(sub2ind(size(phi_z_train), (1:train_set)', labels)) = 1;
x_train=x(1:train_set,:);
x_test=x((train_set+1):end,:);
class_len=length(unique(labels));
L_best=-inf;
restarts=80;

iterations=100;

Emu_train=mean_dir(x_train,labels);

for iter=1:restarts
    phi_z=[phi_z_train; ones(N-train_set,T)/T];
    Ew=zeros(N,1)+0.5; Ew(1:train_set)=0;
    Emu=[Emu_train; rand_dir(T-class_len,d,0)];
    Ealpha=class_len+5; %assuming upto 5 more classes
%     Ekappa=0.5+0.5*rand(T,1); 
    L=zeros(iterations,1);
    for i=1:iterations
        [Ekappa, Elnkappa,kappa_a,kappa_b]=q_kappa(x,phi_z,Ew,Emu);
        [Emu,mu_kappa]=q_mu(x,phi_z,Ew,Ekappa);        
        [lnV, ln1_V,V_a,V_b]=qV(phi_z,Ealpha);
        [Ealpha, a,b]=q_alpha(ln1_V);     
        phi_z=qz(x_test,lnV,ln1_V,Emu,Ekappa,Elnkappa,Ew((train_set+1):end),phi_z_train);
        Ew=qw(x_test,Emu,Ekappa,Elnkappa,phi_z(train_set:end,:),lnp_sticky,train_set);
%         Ew=qw(x,Emu,Ekappa,Elnkappa,phi_z,lnp_sticky);
%         lnp_sticky=qp(Ew((train_set+1):end));
        L(i)=lnlb_t(x,a,b,Ealpha,V_a,V_b,lnV,ln1_V,mu_kappa,Emu,kappa_a,kappa_b,...
                                        Ekappa,Elnkappa,phi_z,Ew,exp(lnp_sticky(1)));
        if i>1
            if (i>15 && abs(L(i)-L(i-1))<1e-5) || (i>15 && abs(L(i)-L(i-1))/L(i-1)<1e-7)
                break;
            end
        end
    end
%     %plot the resulting maximisation of L
%     [~,class]=max(phi_z,[],2);
%     subplot(8,5,iter);plot(L(1:i)); xlabel(length(unique(class)));
    %%%%%%%%%%%%%%%%%%%
    if L(end)>L_best
        L_best=L(end);
        phi_z_best=phi_z;
        Ew_best=Ew;
%         class_best=class;
        Emu_best=Emu;
    end    
end

[~,class_best]=max(phi_z_best,[],2);
Emu_best=Emu_best(:,unique(class_best,'stable'));
% class_best=relabel(class_best);
Emu_best=bsxfun(@rdivide,Emu_best,sqrt(sum(Emu_best.^2)));
pz=bsxfun(@times,phi_z_best,1-Ew_best)+...
    [zeros(1,T); bsxfun(@times,phi_z_best(1:(end-1),:),Ew_best(2:end))];
[~,class2]=max(pz,[],2);

%gamma distribution approximation
function [Ealpha, a,b]=q_alpha(ln1_V)
a=length(ln1_V)+1e-9;
b=1e-9-sum(ln1_V);
Ealpha=a/b;
% Elnalpha=psi(a)-log(b);

%beta distribution approximation
function [lnV, ln1_V, gamma_1,gamma_2]=qV(phi_z,Ealpha)
gamma_1=(sum(phi_z)+1)';
phi_z=cumsum(phi_z,2);
phi_z=bsxfun(@minus,phi_z(:,end),phi_z);
gamma_2=(sum(phi_z)+Ealpha)';
% gamma=[gamma_1 gamma_2];

%expectations
lnV=psi(gamma_1)-psi(gamma_1+gamma_2);
ln1_V=psi(gamma_2)-psi(gamma_1+gamma_2);

%multinomail approxmiation
function p=qz(x,lnV,ln1_V,Emu,Ekappa,Elnkappa,Ew,phi_z_train)
d=size(x,2);
ln1_V=cumsum(ln1_V);
ln1_V=[0; ln1_V(1:(end-1))];

lnVMF=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),(0.5*(d-1)*Elnkappa-Ekappa)');
lnVMF_1=bsxfun(@times,lnVMF,1-Ew);
lnVMF_2=[zeros(1,length(Ekappa)); bsxfun(@times,lnVMF(2:end,:),Ew(2:end))];

p=bsxfun(@plus,lnVMF_1+lnVMF_2,(ln1_V+lnV)');
p=exp(bsxfun(@minus,p,max(p,[],2)));
p=bsxfun(@rdivide, p,sum(p,2));
p=[phi_z_train; p];

%bernoulli approxmiation
function Ew=qw(x,Emu,Ekappa,Elnkappa,phi_z,lnp,train_set)
d=size(x,2);

lnVMF=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),(0.5*(d-1)*Elnkappa-Ekappa)');
p1=sum(phi_z(1:(end-1),:).*lnVMF,2)+lnp(1);
p2=sum(phi_z(2:end,:).*lnVMF,2)+lnp(2);

% p1=sum(phi_z(1:(end-1),:).*lnVMF(2:end,:),2)+lnp(1);
% p2=sum(phi_z(2:end,:).*lnVMF(2:end,:),2)+lnp(2);

p=exp(bsxfun(@minus,[p1 p2],max([p1 p2],[],2)));
p=bsxfun(@rdivide, p,sum(p,2));

Ew=[zeros(train_set,1); p(:,1)];
% Ew=[0; p(:,1)];

% function Elnp=qp(Ew)
% alpha=9; beta=1;
% a=sum(Ew)+alpha;
% b=sum(1-Ew)+beta;
% Elnp=[psi(a)-psi(a+b) psi(b)-psi(a+b)];

%vMF approxmiation
function [Emu,eta_kappa]=q_mu(x,p,Ew,Ekappa)
%prior parameters
kappa=1e-6; d=size(x,2); 
% T=length(Ekappa); 
mu=1./sqrt(d);
prior_const=kappa*mu;

v1=bsxfun(@times,x,1-Ew)'*p;
v2=bsxfun(@times,x(2:end,:),Ew(2:end))'*p(1:(end-1),:);
v=bsxfun(@times,v1+v2,Ekappa')+prior_const;

eta_kappa=sqrt(sum(v.^2));
Emu=bsxfun(@rdivide,v,eta_kappa);

% C=besseli(d/2,eta_kappa)./besseli(d/2-1,eta_kappa);

lb=sqrt(1+((d/2)./eta_kappa).^2)-(d/2)./eta_kappa; 
ub=sqrt(1+((d/2-0.5)./eta_kappa).^2)-(d/2-0.5)./eta_kappa;
C_est=0.5*(lb+ub);
% C(isnan(C))=C_est(isnan(C));

Emu=bsxfun(@times,Emu,C_est)';

%gamma approximation on kappa
function [Ekappa, Elnkappa,a,b]=q_kappa(x,p,Ew,Emu)
d=size(x,2);
p1_w=bsxfun(@times,p,1-Ew);
pw=bsxfun(@times,p(1:(end-1),:),Ew(2:end));
a=((d-1)/2)*(sum(pw)+sum(p1_w))'+1e-9; 

muTx=1-x*Emu';
b=sum(muTx.*p1_w)'+sum(muTx(2:end,:).*pw)'+1e-9; 


Ekappa=a./b;
Elnkappa=psi(a)-log(b);

