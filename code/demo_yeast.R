library(matlab)
library(movMF)
library(clue)

setwd('thesis/dirProcess/code/')

a=read.csv('Spellman.csv')
x=as.matrix(a[,-1])
N=dim(x)[1]
len=sqrt(rowSums(x^2))
x=x / len

max_cluster=100

S_ave=zeros(max_cluster,1)
S_max=zeros(max_cluster,1)
H_ave=zeros(max_cluster,1)
H_min=zeros(max_cluster,1)

for (c in 1:max_cluster){
    b=movMF(x,c,control=list(kappa="Banerjee_et_al_2005"))
       
    class=apply(b$P,1,which.max)
    mu=b$theta
    len=sqrt(rowSums(mu^2))
    mu=mu / len

    mu_per_n=mu[class,]
    
    muTmu=mu%*%t(mu)
    n=zeros(dim(mu)[1],1)
    for (i in 1:dim(mu)[1])
        n[i]=sum(class==i);
    nTn= n%*%t(n) 
    muTmu[seq(1,length(muTmu),dim(muTmu)[1]+1)]=0
    nTn[seq(1,length(nTn),dim(nTn)[1]+1)]=0
    S_ave[c]=base::sum(nTn*muTmu)/base::sum(nTn)
    S_max[c]=max(muTmu)
    
    cluster_dot_prod=rowSums(mu_per_n*x)
    H_min[c]=min(cluster_dot_prod)
    H_ave[c]=base::sum(cluster_dot_prod)/N
}

plot(H_min)
x11(); plot(H_ave)
x11(); plot(S_max)
x11(); plot(S_ave)

res=rbind(t(H_min),t(H_ave),t(S_max),t(S_ave))
write.csv(res, file = "gene_banjeree.csv")
