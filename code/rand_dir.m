function dir=rand_dir(n,d,pos)

means=randn(n,d);
if pos==1
 means=abs(means);
end
len=sqrt(sum(means.^2,2));
dir=bsxfun(@rdivide,means,len);