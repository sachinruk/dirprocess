clear all
close all
clc

% s=trnd(3,20000,2);
s=randn(20000,2);

A=[1 1; 0 2];
x=s*A;
figure; plot(x(:,1),x(:,2),'.');

x(x(:,1)<0,:)=-x(x(:,1)<0,:);
figure; plot(x(:,1),x(:,2),'.')

x = convert_to_unit(x);
figure; plot(x(:,1),x(:,2),'.')



[phi_z2,class2,Eeta2]=EM(x);

hold on;
for i=1:size(Eeta2,2)
    v=Eeta2(:,i);
    if v(1)<0
        v=-v;
    end
    plot(v(1),v(2),'rx');
    pause;
end
for i=1:2
    v=A(i,:); v=v./norm(v);
    plot(v(1),v(2),'gx');
end

[phi_z,class_best,Eeta_best,llb,class_hierarchy,x2]=EM_nested(x,3);
u=unique(class_best{4});
Eeta3=Eeta_best{3}(u,:)';

figure;
hold on;
for i=1:size(Eeta3,2)
    v=Eeta3(:,i);
    if v(1)<0
        v=-v;
    end
    plot(v(1),v(2),'rx');
end
for i=1:2
    v=A(i,:); v=v./norm(v);
    plot(v(1),v(2),'gx');
end