close all
clear all
clc

rng(1);
kappa=[50 5000]; n=[15 100];
true_directions=[[1; 1; 1] -[1; 1; 1]]; %[-0.5; 4; 1]
mu=randvonMisesFisherm(3, n(1), kappa(1), true_directions(:,1))';
x=[];
for i=1:n(1)
    x=[x; randvonMisesFisherm(3, n(2), kappa(2), mu(i,:))'];
end

mu2=randvonMisesFisherm(3, n(1), kappa(1), true_directions(:,2))';
x2=[];
for i=1:n(1)
    x2=[x2; randvonMisesFisherm(3, n(2), kappa(2), mu2(i,:))'];
end

data=[x; x2];

figure; plot3(data(:,1),data(:,2),data(:,3),'.'); plotSphere();
view(45,45);
% [phi_z_best,class_best,Eeta1_best]=EM(data);
% figure; hold on;
% plotSphere();
% u=unique(class_best);
% c=jet(length(u));
% for i=1:length(u)
%     plot3(data(class_best==u(i),1),data(class_best==u(i),2),data(class_best==u(i),3),'.','color',c(i,:));
% end
levels=3;
[phi_z,class,Eeta,llb,class_hierarchy,data2, Ekappa]=EM_nested(data,levels);

figure; hold on;
plotSphere();
u=unique(class_hierarchy(:,levels));
c=jet(length(u));
for i=1:length(u)
    idx=class_hierarchy(:,levels)==u(i);
    plot3(data(idx,1),data(idx,2),data(idx,3),'.','color',c(i,:));
end
view(45,45);
figure; hold on;
plotSphere();
u=unique(class_hierarchy(:,levels-1));
c=jet(length(u));
for i=1:length(u)
    idx=class_hierarchy(:,levels-1)==u(i);
    plot3(data(idx,1),data(idx,2),data(idx,3),'.','color',c(i,:));
end
view(45,45);
% u=unique(class_hierarchy(:,3));
% u_mu=Eeta{4}(u,:);
% u_mu=normalise(u_mu);
% [phi_z_best,class_best,Eeta1_best]=EM(u_mu);