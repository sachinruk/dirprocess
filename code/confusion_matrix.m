function C=confusion_matrix(c,l)

c=relabel(c);
l=relabel(l);
C=zeros(max(l),max(c));

for i=1:max(l)
    for j=1:max(c)
        C(i,j)=sum(c(l==i)==j);
    end
end