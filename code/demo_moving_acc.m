clear all
close all
clc

load All_Sensor_Features_and_Labels.mat

x=[]; labels=[];
for i=1:length(Feature_Matrix_mat)
    x=[x; Feature_Matrix_mat{i}'];
    labels=[labels; Feature_Label{i}'];
end
labels(labels==0)=[];

w=convert_to_unit(x);
% labels=labels(2:end);
train_set=800;
%unsupervised vMF
rng(1);
[phi_z,class,Eeta,llb]=EM(w);
switches1=length(class)-sum(diff(class)==0);

disp([nmi3(class((train_set+1):end),labels((train_set+1):end)) RandIndex(class((train_set+1):end),labels((train_set+1):end))]);
C1=confusion_matrix(class,labels);

%semi-supervised vMF
rng(1);
[phi_z4,class2,Eeta4,llb4]=EM_semiSupervised(w,labels(1:train_set),0);
disp([nmi3(class2((train_set+1):end),labels((train_set+1):end)) RandIndex(class2((train_set+1):end),labels((train_set+1):end))]);
C2=confusion_matrix(class2((train_set+1):end),labels((train_set+1):end));

%unsupervised with time consistency
rng(1);
p=0.99;
[phi_z2,Ew2,class3,class3_t,Eeta2]=EM_t(w,p);
switches2=length(class3_t)-(sum(diff(class3_t)==0));

disp([nmi3(class3_t,labels) RandIndex(class3_t,labels)]);

%semisupervised with time consistency
rng(1);
p=log([p 1-p]);
[phi_z3,Ew3,class4,class4_t,Eeta3]=EM_t_ss(w,p,labels(1:train_set));
switches3=length(class4_t)-(sum(diff(class4_t)==0));

disp([nmi3(class4_t((train_set+1):end),labels((train_set+1):end)) RandIndex(class4_t((train_set+1):end),labels((train_set+1):end))]);

%look at the accuracies of the two models
acc_normal=sum(class2((train_set+1):end)==labels((train_set+1):end))/length(labels((train_set+1):end));
acc_t=sum(class4_t((train_set+1):end)==labels((train_set+1):end))/length(labels((train_set+1):end));

%k-means 
idx=kmeans(x,4);
disp([nmi3(labels,idx) RandIndex(labels,idx)]);

idx2=kmeans(w,4,'Distance','cosine');
disp([nmi3(labels,idx2) RandIndex(labels,idx2)]);

% idx3=kmeans(w,4,'Distance','correlation');
% disp([nmi3(labels,idx3) RandIndex(labels,idx3)]);

% %random chance of happening
% for i=1:train_set
%     dummy=randsample(12,length(labels),true);
%     acc(i)=sum(dummy==labels)/length(labels);
% end
% 
% figure; hist(acc,20);
