close all
clear all
clc

theta2=0.01:0.0025:(pi/2);
theta2_deg=theta2*180/pi;
x2=[cos(theta2) sin(theta2)];
m=[1 1;1 -1;-1 1;-1 -1];
p2=0;
for i=1:4
p2=p2+exp(kappa*(m(i,:)*x2));
end

figure; polar(theta2,p2);
figure; plot(theta2_deg,p2);