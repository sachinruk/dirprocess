function accuracy(c1,c2)

c1=relabel(c1);
c2=relabel(c2);
C=zeros(max(c2),max(c1));

for i=1:max(c2)
    for j=1:max(c1)
        C(i,j)=sum(c1(c2==i)==j);
    end
end