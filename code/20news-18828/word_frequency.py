#!/usr/bin/env python3

import argparse
import codecs
import email
import email.parser
import os
import re
import sys


g_document_id = 0
g_output_file = open("word_counts.txt", "w")
g_label_file = open("file_labels.txt", "w")
g_vocabulary = {}


def directory_indices(folder):
    """Returns a name to index mapping for folder names.
    
    :param folder the root folder to process
    :return dictionary mapping folder name to index
    """
    dir2idx = {}
    for i, name in enumerate(sorted(os.listdir(folder))):
        dir2idx[name] = i

    return dir2idx


def word_indices(fname):
    """Returns a name to index mapping for the vocabulary words.

    :param fname file containing the vocabulary
    :return dictionary mapping word to index
    """
    vocabulary = []
    for line in open(fname).readlines():
        vocabulary.append(line.strip())

    word2idx = {}
    for i, word in enumerate(sorted(vocabulary)):
        word2idx[word] = i
    return word2idx


def parse_folder(folder, idx):
    for root, dirs, files in os.walk(folder):
        for fname in sorted(files):
            parse_file(os.path.join(root, fname), idx)


def parse_file(fname, idx):
    mail = email.message_from_file(codecs.open(
        fname,
        "r",
        "ISO-8859-1"
    ))

    global g_vocabulary
    words = re.split("\W+", mail.get_payload())
    word_counts = {}
    for word in words:
        word = word.lower()
        if len(word) < 2 or word.isdigit():
            continue
        if word not in g_vocabulary:
            g_vocabulary[word] = len(g_vocabulary)

        word_counts[word] = word_counts.get(word, 0) + 1

    global g_document_id, g_output_file, g_label_file
    for word, count in word_counts.items():
        g_output_file.write("{} {} {} {}\n".format(
            idx,
            g_document_id,
            g_vocabulary[word],
            count
        ))
    g_label_file.write("{} {} {}\n".format(idx, g_document_id, fname))
    g_document_id += 1


def main():
    parser = argparse.ArgumentParser(description="Parse news20 dataset")
    parser.add_argument("folder", help="The folder containin the files to process")

    args = parser.parse_args()

    dir2idx = directory_indices(args.folder)

    # Process each folder
    for folder, idx in dir2idx.items():
        parse_folder(os.path.join(args.folder, folder), idx)

    # Save directory and vocabulary indices to file
    with open("folder_indices.txt", "w") as out:
        for folder, idx in sorted(dir2idx.items(), key=lambda x: x[1]):
            out.write("{} {}\n".format(idx, folder))
    with open("vocabulary_indices.txt", "w") as out:
        for word, idx in sorted(g_vocabulary.items(), key=lambda x: x[1]):
            out.write("{} {}\n".format(idx, word))



if __name__ == "__main__":
    sys.exit(main())
