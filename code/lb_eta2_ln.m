function [Eeta2, Eeta2sq]=lb_eta2_ln(C1,C2)
k=C2.^2./(4*C1);
k2=(2*C1)./(C2.^2);

sigma2_est=(k2-1)-sqrt(k2-1);

opts = optimset('MaxIter',100,...
                'Diagnostics', 'off',...
                'GradObj', 'on',...
                'Display', 'off',...
                'TolFun',  1e-10,...
                'MaxFunEvals', 100,...
                'Hessian','on',...
                'DerivativeCheck','on');
% opts.Display = 'on'; opts.MaxIter=200; opts.MaxFunEvals=100;
% opts.TolFun=1e-10; opts.GradObj='on';
T=1000; 
par=zeros(T,1); L=zeros(T,1);
for t=1:T
    [par(t), L(t)]=fminunc(@(params)optimise(params,k(t)),...
                                                log(sigma2_est(t)),opts);
    if mod(t,100)==0
        disp(t);
    end
end
sigma2=exp(par);
mu=log(C2./(2*C1))-3*sigma2/2;
Eeta2=exp(mu+sigma2/2);
Eeta2sq=exp(2*mu+2*sigma2);

function [L, dL_dlnsigma2, d2L_dlnsigma22]=optimise(lnsigma2,k)
sigma2=exp(lnsigma2);

k_exp_sigma2=k*exp(-sigma2);
L=k_exp_sigma2+0.5*lnsigma2;
dL_dlnsigma2=-k_exp_sigma2.*sigma2+0.5;
d2L_dlnsigma22=k_exp_sigma2.*(exp(2*lnsigma2)-sigma2);

L=-L; dL_dlnsigma2=-dL_dlnsigma2; d2L_dlnsigma22=-d2L_dlnsigma22;