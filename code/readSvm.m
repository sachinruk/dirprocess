fid=fopen('train.svm','r');

tline=fgetl(fid); %ignore first line

x=[];
class=zeros(1e4,1);
k=1;
while ischar(tline)
    tline = fgetl(fid);  
    if tline==-1
        break;
    end
    features=strsplit(tline(1:(end-1)),{':',' '});    
    features=str2double(features(1:(end-1)));
    class(k)=features(1);
    lenf=(length(features)-1)/2;
    x=[x; [k+zeros(lenf,1) features(2:2:end)' features(3:2:end)']];
    k=k+1;
end
fclose(fid);
x(:,2)=x(:,2)+1;
x=sparse(x(:,1),x(:,2),x(:,3));