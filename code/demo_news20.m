clear all
close all
clc

load news20_full2.mat

% label_num=wordcounts(:,1);
news20=sparse(wordcounts2(:,2),wordcounts2(:,3),wordcounts2(:,4));
u=unique(wordcounts2(:,1:2),'rows','stable');
label_num=zeros(size(news20,1),1);
label_num(u(:,2))=u(:,1);

% %clear up vocabulary words that are not used much
idx=sum(news20)<20;
news20(:,idx)=[];

%clear up empty documents - 66 of them
idx=sum(news20,2)==0;
news20=news20(~idx,:);
label_num=label_num(~idx);



% idx=(sum(train_data2)==0);
% train_data2(:,idx)=[];
w=tfidf(news20);
w2 = convert_to_unit(w);
sparsity=sum(w2(:)~=0)/length(w2(:));

% label_num=train_label;

%completely unsupervised
rng(1);
[phi_z_best,class_best,Eeta1_best,L_best]=EM(w2);
disp([nmi3(class_best,label_num), RandIndex(class_best,label_num)]);

%train on half
rng(1);
idx=randperm(size(w2,1)); %so that we manage to sample a full set of labels
w2_train=w2(idx,:); labels2=label_num(idx);
training=floor(length(labels2)/2);
labels_train=labels2(1:training);
[phi_z2,class2,Eeta2,L_best2]=EM_semiSupervised(w2_train,labels_train,1);
disp([nmi3(class2((training+1):end),labels2((training+1):end)),...
    RandIndex(class2((training+1):end),labels2((training+1):end))]);

%k-means 
rng(1);
[~,idx]=k_means_normal(w,30,100,1);
disp([nmi3(label_num,idx) RandIndex(label_num,idx)])

rng(1);
idx2=kmeans(w2,length(unique(class_best)),'Distance','cosine');
disp([nmi3(label_num,idx2) RandIndex(label_num,idx2)])

rng(1);
idx3=kmeans(w2,length(unique(label_num)),'Distance','cosine');
disp([nmi3(label_num,idx3) RandIndex(label_num,idx3)])

rng(1);
idx4=kmeans(w2,5,'Distance','cosine');
disp([nmi3(label_num,idx4) RandIndex(label_num,idx4)])
