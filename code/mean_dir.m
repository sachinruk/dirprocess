function Emu_train=mean_dir(x_train,labels)
u=unique(labels);
Emu_train=zeros(length(u),size(x_train,2));
for i=1:length(u)
    Emu_train(i,:)=mean(x_train(labels==u(i),:));
end   