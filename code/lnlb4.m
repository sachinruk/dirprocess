%lower bound to likelihood
function [L]=lnlb4(x,a,b,Ealpha, V_a,V_b,lnV,ln1_V, mu_kappa,Emu,...
                                kappa_a,kappa_b,Ekappa,Elnkappa, u, phi_z)
% a=length(ln1_V)+1e-6;
% b=1e-6-sum(ln1_V);
lnpalpha=-1e-6*Ealpha;
lnqalpha=a*log(b)-gammaln(a)-b*Ealpha;
lb_alpha=lnpalpha-lnqalpha;
% Ealpha=a/b;

% occ_clusters=(sum(phi_z)>0); %occupied clusters

gamma_1=(sum(phi_z))';
phi_z2=cumsum(phi_z,2);
gamma_2=sum(1-phi_z2)';
lnpV=gamma_1.*lnV+gamma_2.*ln1_V+(Ealpha-1)*ln1_V;
lnqV=gammaln(V_a+V_b)-(gammaln(V_a)+gammaln(V_b))+(V_a-1).*lnV+(V_b-1).*ln1_V;
lb_V=lnpV-lnqV;
% lb_V=sum(lb_V(occ_clusters));
lb_V=sum(lb_V);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N,d]=size(x); nu=d/2-1; kappa=1e-6; mu=1./sqrt(d); prior_const=kappa*mu;
C=nu+0.5;
lnpmu=sum(prior_const*Emu');
sqrt_part=sqrt(mu_kappa.^2+C^2);
lb_lnqmu=-sqrt_part+C*log(sqrt_part+C); %verified- normalising constant

%mu part of q(mu)
% C2=besseli(d/2,mu_kappa)./besseli(d/2-1,mu_kappa);
lb=sqrt(1+((d/2)./mu_kappa).^2)-(d/2)./mu_kappa; 
ub=sqrt(1+((d/2-0.5)./mu_kappa).^2)-(d/2-0.5)./mu_kappa;
C_est=0.5*(lb+ub);
% C2(isnan(C2))=C_est(isnan(C2));
lb_lnqmu=lb_lnqmu+mu_kappa.*C_est; %because mu^Tmu=1

lb_lb_mu=lnpmu-lb_lnqmu;
% lb_lb_mu=sum(lb_lb_mu(occ_clusters));
lb_lb_mu=sum(lb_lb_mu);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% kappa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lnpkappa=(1e-9-1).*Elnkappa-1e-9*Ekappa;
lnqkappa=kappa_a.*log(kappa_b)-gammaln(kappa_a)+(kappa_a-1).*Elnkappa-kappa_b.*Ekappa;
lb_kappa=lnpkappa-lnqkappa;
% lb_kappa=sum(lb_kappa(occ_clusters));
lb_kappa=sum(lb_kappa);

%%%%%%%%%%%%%%%%%%%%%%%%%%
% z_n
%%%%%%%%%%%%%%%%%%%%%%%%
lnqz=phi_z.*log(phi_z);
lnqz=-sum(lnqz(phi_z>0));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  p(x_n|z_n)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ln_exp_u_term=log(1+sqrt(1+exp(u)));
extreme_idx=u>500;
ln_exp_u_term(extreme_idx)=u(extreme_idx)/2;
constant=-C+2*C*log(C)-(d/2)*log(2*pi);
factor=1-1./(sqrt(exp(u)+1));
c_kappa=-Ekappa+C*ln_exp_u_term+C*factor.*(Elnkappa-log(C)-0.5*u);
lnpx=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),c_kappa').*phi_z;
lnpx=sum(lnpx(:))+N*constant;
% lb_z=-sum(lb_z);

L=lnpx+lb_alpha+lb_V+lb_lb_mu+lb_kappa+lnqz;
% lik=[L,lnpx,lb_alpha,lb_V,lb_lb_mu,lb_kappa,lnqz];