function Y = tfidf( x )
% FUNCTION applies TF-IDF weighting to word count vector matrix.
%
%   [Y w] = tfidf2( X );
%
% INPUT :
%   X        - word count vectors (size is documents x words)
%
% OUTPUT :
%   Y        - TF-IDF weighted document-term matrix

% TF * IDF
Y = bsxfun(@times,tf(x),idf(x));

function w = tf( x )
% SUBFUNCTION computes word frequencies

% Y = X ./ repmat( sum(X,1), size(X,1), 1 );
% Y( isnan(Y) ) = 0;
w=spfun(@(x)(log(x)+1),x);


function I = idf(x)
% SUBFUNCTION computes inverse document frequencies

% number of documents with term t
nz = sum(x > 0);
% number of documents
N=size(x,1);
% compute idf for each document
I = log(N./nz);