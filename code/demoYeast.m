close all;clear all; clc;

maxDepth = 3;

%load data
load yeast.mat
%unit
x=bsxfun(@minus,Spellman,mean(Spellman,2));
x = convert_to_unit(x);

rng(1); levels=3;
% figure;
[phi_z,class,Eeta,llb,class_hierarchy,x3]=EM_nested(x,levels);

[phi_z2,class2,Eeta2]=EM(x);

%rearrange x
x2=[];
for i=1:max(class2)
    x2=[x2; x(class2==i,:)];
end
figure; subplot(131); imagesc(x); xlabel('data without rearranging');
subplot(132); imagesc(x2); xlabel('single level clustering')
subplot(133); imagesc(x3); xlabel('hierarchical clustering')


%homogeneity measure
L=levels+2;
mean_vector=Eeta{L-1}(class{L},:);
H=mean_vector.*x;
H_min=min(sum(H,2));
H_ave=sum(H(:))/size(x,1);

H2=Eeta2(class2,:).*x;
H_min2=min(sum(H2,2));
H_ave2=sum(H2(:))/size(x,1);


%seperation measure
u_classes=unique(class{L},'stable');
u_mean=Eeta{L-1}(u_classes,:);
n_mu=zeros(length(u_classes),1);
for i=1:length(u_classes)
    n_mu(i)=sum(class{L}==u_classes(i));
end
n_matrix=n_mu*n_mu';
n_matrix(1:length(n_matrix)+1:end)=0;
mu_matrix=u_mean*u_mean';
mu_matrix(1:length(mu_matrix)+1:end)=0;
S=mu_matrix.*n_matrix;
S_ave=sum(S(:))/sum(n_matrix(:));
S_max=max(mu_matrix(:));

u_classes2=unique(class2,'stable');
u_mean2=Eeta2;
n_mu2=zeros(length(u_classes2),1);
for i=1:length(u_classes2)
    n_mu2(i)=sum(class2==u_classes2(i));
end
n_matrix2=n_mu2*n_mu2';
n_matrix2(1:length(n_matrix2)+1:end)=0;
mu_matrix2=u_mean2*u_mean2';
mu_matrix2(1:length(mu_matrix2)+1:end)=0;
S2=mu_matrix2.*n_matrix2;
S_ave2=sum(S2(:))/sum(n_matrix2(:));
S_max2=max(mu_matrix2(:));

disp([H_ave, H_min, S_ave, S_max ;
    H_ave2, H_min2, S_ave2, S_max2]);

load genebanjeree.mat
c=length(unique(class_hierarchy(:,3)));
c2=length(u_classes2);

figure; plot(81:100, genebanjeree(1,81:100),'LineWidth',4); 
hold on; plot(c,H_min,'r+','LineWidth',2); 
plot(c2,H_min2,'k*','LineWidth',10); xlabel('Number of clusters');
figure; plot(81:100, genebanjeree(2,81:100),'LineWidth',4); 
hold on; plot(c,H_ave,'r+','LineWidth',2); 
plot(c2,H_ave2,'k*','LineWidth',10); xlabel('Number of clusters');
figure; plot(81:100, genebanjeree(3,81:100),'LineWidth',4); 
hold on; plot(c,S_max,'r+','LineWidth',2); 
plot(c2,S_max2,'k*','LineWidth',10); xlabel('Number of clusters');
figure; plot(81:100, genebanjeree(4,81:100),'LineWidth',4); 
hold on; plot(c,S_ave,'r+','LineWidth',2); 
plot(c2,S_ave2,'k*','LineWidth',10); xlabel('Number of clusters');