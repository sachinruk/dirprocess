close all
clear all
clc

rng(1);
figure; hold on;
kappa=50;
x=randvonMisesFisherm(2, 100, kappa, [1; 1])';
plot(x(:,1),x(:,2),'.')

x2=randvonMisesFisherm(2, 100, kappa, -[1; 1])';
plot(x2(:,1),x2(:,2),'r.')

x3=randvonMisesFisherm(2, 100, kappa, [-0.5; 4])';
plot(x3(:,1),x3(:,2),'g.')

x4=randvonMisesFisherm(2, 100, kappa, [-2; 0.5])';
plot(x4(:,1),x4(:,2),'k.')

x=[x; x2; x3; x4];
figure; plot(x(:,1),x(:,2),'.');

[phi_z_best,class_best,Eeta1_best]=EM(x);
figure; hold on;
u=unique(class_best);
c=jet(length(u));
for i=1:length(u)
    plot(x(class_best==u(i),1),x(class_best==u(i),2),'.','color',c(i,:));
end

true_directions=[[1; 1] -[1; 1] [-0.5; 4] [-2; 0.5]];
%normalise
len=sqrt(sum(true_directions.^2));
true_directions=bsxfun(@rdivide,true_directions,len);
%dot product with inferred directions
dot_prod=diag(true_directions'*Eeta1_best');

% c=zeros(1000,1);
% d=10000;
% for i=1:1000
%     a=rand(d,1);
%     a=a/norm(a);
%     b=rand(d,1);
%     b=b/norm(b);
%     c(i)=a'*b;
% end
% figure; hist(c,50)
% 
% 
% c=zeros(1000,1);
% 
% for i=1:1000
%     a=randn(d,1);
%     a=a/norm(a);
%     b=randn(d,1);
%     b=b/norm(b);
%     c(i)=a'*b;
% end
% figure; hist(c,50)