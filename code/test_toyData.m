clear all
close all
clc

rng(1);
[x,mu]=syntheticgen(10,6,100,20);
[class,eta_mu]=EM(x);
eta_mu2=eta_mu(:,unique(class));
dot_prod=mu*eta_mu2;
disp(acos(max(dot_prod'))*180/pi);
disp(acos(min(dot_prod'))*180/pi);
disp(length(unique(class)))