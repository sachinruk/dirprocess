function [ points,mu] = syntheticgen(dim, numbClusters, numbPointsPerCluster, kappa)
% Generate synthetic data
% [ points ] = syntheticgen(dim, numbClusters, numbPointsPerCluster, kappa)
%      dim: Dimension of each data
%      numbClusters = Number of clusters to generate. Each cluster = 1 \mu
%      numbPointsPerCluster = clear
%      kappa = 1 value for all clusters (just to make it easier)

rng('default');

K = numbClusters;


if (nargin<4)
   %Create kappa: 1 kappa for all mu (same value)
    kappa = 2500;    %to make it focuses 
end;

%Create K mu
mu = randn(K,dim);
mu = bsxfun(@rdivide,mu,norm(mu));  %convert to unit vector

points = [];    %row: point, cols:features, each is unit vector

for k=1:K
	[ X ] = randvonMisesFisherm(dim, numbPointsPerCluster, kappa, mu(k,:));
	points = [ points; X' ];
end;

