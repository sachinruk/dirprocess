%lower bound to likelihood
function [lik]=lnlb2(x,phi_z,ln1_V,Ealpha,eta_kappa,Eeta1,Eeta2)
a=length(ln1_V)+1e-6;
b=1e-6-sum(ln1_V);
lb_alpha=gammaln(a)-a*log(b);

occ_clusters=(sum(phi_z)>0); %occupied clusters

gamma_1=(sum(phi_z)+1)';
phi_z2=cumsum(phi_z,2);
phi_z2=bsxfun(@minus,phi_z2(:,end),phi_z2);
gamma_2=(sum(phi_z2)+Ealpha)';
lb_V=gammaln(gamma_1)+gammaln(gamma_2)-gammaln(gamma_1+gamma_2)-Ealpha*ln1_V;
lb_V=sum(lb_V(occ_clusters));

p=size(x,2); nu=p/2-1;

% lgIeta_kappa=log(besseli(nu,eta_kappa));
% lgIeta_kappa_large=eta_kappa-0.5*log(2*pi*eta_kappa); %for large argument
% lgIeta_kappa(eta_kappa>200)=lgIeta_kappa_large(eta_kappa>200);
% lb_eta1=sum(lgIeta_kappa-nu*log(eta_kappa));
sqrt_part=sqrt(eta_kappa.^2+(nu+1).^2);
lb_lb_eta1=sqrt_part+(nu+1)*log(sqrt_part-(nu+1))-2*(nu+1)*log(eta_kappa);
lb_lb_eta1=sum(lb_lb_eta1(occ_clusters));

eta2_a=((p-1)/2)*sum(phi_z)'+1e-9; 
eta2_b=sum((1-x*Eeta1).*phi_z)'+1e-9; % C2=sum((p'*x)'.*eta_mu)';<- can be used instead
norm_const=-eta2_a.*log(eta2_b)+gammaln(eta2_a);
lb_eta2=sum(norm_const(occ_clusters))-sum(phi_z.*(x*Eeta1))*Eeta2;

lb_z=phi_z.*log(phi_z);
lb_z=-sum(lb_z(occ_clusters));

L=lb_alpha+lb_V+lb_lb_eta1+lb_eta2+lb_z;
lik=[L,lb_alpha,lb_V,lb_lb_eta1,lb_eta2,lb_z];