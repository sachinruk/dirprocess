function [phi_z_best,Ew,class_best,class2,Emu_best]=EM_t(x,p_sticky)
T=100; N=size(x,1);

L_best=-inf;
restarts=40;
iterations=100;
for iter=1:restarts
    phi_z=ones(N,T)/T;
    Ew=zeros(N,1)+0.5; Ew(1)=0;
    Emu=k_means(x,T,20,1);
    Ealpha=10*rand;
     
    L=zeros(iterations,1);
    for i=1:iterations        
        [Ekappa, Elnkappa,kappa_a,kappa_b]=q_kappa(x,phi_z,Ew,Emu);    
        [Emu,mu_kappa]=q_mu(x,phi_z,Ew,Ekappa);
        [lnV, ln1_V,V_a,V_b]=qV(phi_z,Ealpha);
        [Ealpha, a,b]=q_alpha(ln1_V);     
        phi_z=qz(x,lnV,ln1_V,Emu,Ekappa,Elnkappa,Ew);
        Ew=qw(x,Emu,Ekappa,Elnkappa,phi_z,p_sticky);
        L(i)=lnlb_t(x,a,b,Ealpha,V_a,V_b,lnV,ln1_V,mu_kappa,Emu,kappa_a,kappa_b,...
                                        Ekappa,Elnkappa,phi_z,Ew,p_sticky);
        if i>1
            if i>15 && abs(L(i)-L(i-1))<1e-9
                break;
            end
        end
    end
    
%     subplot(8,5,iter);plot(L); xlabel(length(unique(class)));
    if L(end)>L_best
        L_best=L(end);
        phi_z_best=phi_z;
%         class_best=class;
        Emu_best=Emu;
    end    
end

% [~,class_best]=max(phi_z_best,[],2);
% Emu_best=Emu_best(:,unique(class_best,'stable'));
% class_best=relabel(class_best);
[~,class_best]=max(phi_z,[],2);
Emu_best=bsxfun(@rdivide,Emu_best,sqrt(sum(Emu_best.^2)));
pz=bsxfun(@times,phi_z,1-Ew)+[zeros(1,T); bsxfun(@times,phi_z(1:(end-1),:),Ew(2:end))];
[~,class2]=max(pz,[],2);

%gamma distribution approximation
function [Ealpha, a,b]=q_alpha(ln1_V)
a=length(ln1_V)+1e-6;
b=1e-6-sum(ln1_V);
Ealpha=a/b;
% Elnalpha=psi(a)-log(b);

%beta distribution approximation
function [lnV, ln1_V, gamma_1,gamma_2]=qV(phi_z,Ealpha)
gamma_1=(sum(phi_z)+1)';
phi_z=cumsum(phi_z,2);
phi_z=bsxfun(@minus,phi_z(:,end),phi_z);
gamma_2=(sum(phi_z)+Ealpha)';
% gamma=[gamma_1 gamma_2];

%expectations
lnV=psi(gamma_1)-psi(gamma_1+gamma_2);
ln1_V=psi(gamma_2)-psi(gamma_1+gamma_2);

%multinomail approxmiation
function p=qz(x,lnV,ln1_V,Emu,Ekappa,Elnkappa,Ew)
d=size(x,2);
ln1_V=cumsum(ln1_V);
ln1_V=[0; ln1_V(1:(end-1))];

lnVMF=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),(0.5*(d-1)*Elnkappa-Ekappa)');
lnVMF_1=bsxfun(@times,lnVMF,1-Ew);
lnVMF_2=[zeros(1,length(Ekappa)); bsxfun(@times,lnVMF(2:end,:),Ew(2:end))];

p=bsxfun(@plus,lnVMF_1+lnVMF_2,(ln1_V+lnV)');
p=exp(bsxfun(@minus,p,max(p,[],2)));
p=bsxfun(@rdivide, p,sum(p,2));

%bernoulli approxmiation
function Ew=qw(x,Emu,Ekappa,Elnkappa,phi_z,p)
d=size(x,2);

lnVMF=bsxfun(@plus,bsxfun(@times,x*Emu',Ekappa'),(0.5*(d-1)*Elnkappa-Ekappa)');
p1=sum(phi_z(1:(end-1),:).*lnVMF(2:end,:),2)+log(p);
p2=sum(phi_z(2:end,:).*lnVMF(2:end,:),2)+log(1-p);

p=exp(bsxfun(@minus,[p1 p2],max([p1 p2],[],2)));
p=bsxfun(@rdivide, p,sum(p,2));

Ew=[0; p(:,1)];

%vMF approxmiation
function [Emu,mu_kappa]=q_mu(x,p,Ew,Ekappa)
%prior parameters
kappa=1e-6; d=size(x,2); 
% T=length(Ekappa); 
mu=1./sqrt(d);
prior_const=kappa*mu;

v1=bsxfun(@times,x,1-Ew)'*p;
v2=bsxfun(@times,x(2:end,:),Ew(2:end))'*p(1:(end-1),:);
v=bsxfun(@times,v1+v2,Ekappa')+prior_const;

mu_kappa=sqrt(sum(v.^2));
Emu=bsxfun(@rdivide,v,mu_kappa);

C=besseli(d/2,mu_kappa)./besseli(d/2-1,mu_kappa);

lb=sqrt(1+((d/2)./mu_kappa).^2)-(d/2)./mu_kappa; 
ub=sqrt(1+((d/2-0.5)./mu_kappa).^2)-(d/2-0.5)./mu_kappa;
C_est=0.5*(lb+ub);
C(isnan(C))=C_est(isnan(C));

Emu=bsxfun(@times,Emu,C)';

%gamma approximation on kappa
function [Ekappa, Elnkappa,a,b]=q_kappa(x,p,Ew,Emu)
d=size(x,2);
p1_w=bsxfun(@times,p,1-Ew);
pw=bsxfun(@times,p(1:(end-1),:),Ew(2:end));
a=((d-1)/2)*(sum(pw)+sum(p1_w))'+1e-9; 

muTx=1-x*Emu';
b=sum(muTx.*p1_w)'+sum(muTx(2:end,:).*pw)'+1e-9; % C2=sum((p'*x)'.*Emu)';<- can be used instead


Ekappa=a./b;
Elnkappa=psi(a)-log(b);

