function Elb=lb_c(C,a1,a2,b,w)

Ex_C1=a1./(b*C);
Ex_C2=a2./(b*C);
varX_C1=a1./(b*C).^2;
varX_C2=a2./(b*C).^2;
Ex3_C1=2*a1./(b*C).^3;
Ex3_C2=2*a2./(b*C).^3;
Ex4_C1=a1.*(6+3*a1)./(b*C).^4;
Ex4_C2=a2.*(6+3*a2)./(b*C).^4;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%for \log(\sqrt((x/c)^2+1)+1):
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k1=Ex_C1.^2+1;
y0=log(sqrt(k1)+1);
% y1=Ex_C1./(Ex_C1.^2+sqrt(k1)+1);
y2=1./(sqrt(k1)+1)-Ex_C1.^2./(k1).^1.5;
y3=Ex_C1.*(3*Ex_C1.^2-2*(k1)-((k1)./(sqrt(k1)+1)).^2)./k1.^2.5;
y4=-6*Ex_C1.^6+6*(7*sqrt(k1)+6).*Ex_C1.^2-9*(sqrt(k1)+1)+6*(sqrt(k1)+4).*Ex_C1.^4./(k1.^3.5.*(sqrt(k1)+1).^3);
log_lb1=y0+y2.*varX_C1./2+y3.*Ex3_C1./6+y4.*Ex4_C1./24;


k2=Ex_C2.^2+1;
y0=C*log(sqrt(k2)+1);
% y1=Ex_C1./(Ex_C2.^2+sqrt(Ex_C2)+1);
y2=1./(sqrt(k2)+1)-Ex_C2.^2./(k2).^1.5;
y3=Ex_C2.*(3*Ex_C2.^2-2*(k2)-((k2)./(sqrt(k2)+1)).^2)./k2.^2.5;
y4=-6*Ex_C2.^6+6*(7*sqrt(k2)+6).*Ex_C2.^2-9*(sqrt(k2)+1)+6*(sqrt(k2)+4).*Ex_C2.^4./(k2.^3.5.*(sqrt(k2)+1).^3);
log_lb2=y0+y2.*varX_C2./2+y3.*Ex3_C2./6+y4.*Ex4_C2./24;

loglb=w.*log_lb1+(1-w).*log_lb2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
%for -sqrt((x/c)^2+1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
y0=sqrt(k1);
% y1=Ex_C1./y0;
y2=1./y0.^3;
y3=-3.*Ex_C1./y0.^5;
y4=3*(4.*Ex_C1.^2-1)./y0.^7;
sqrt_lb1=y0+y2.*varX_C1./2+y3.*Ex3_C1./6+y4.*Ex4_C1./24;

y0=sqrt(k2);
% y1=Ex_C1./y0;
y2=1./y0.^3;
y3=-3.*Ex_C2./y0.^5;
y4=3*(4.*Ex_C2.^2-1)./y0.^7;
sqrt_lb2=y0+y2.*varX_C1./2+y3.*Ex3_C1./6+y4.*Ex4_C1./24;

sqrt_lb=w.*sqrt_lb1+(1-w).*sqrt_lb2;

Elb=C.*(loglb-sqrt_lb);

