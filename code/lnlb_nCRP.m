%lower bound to likelihood
function [L]=lnlb_nCRP(x, Eeta, eta_kappa,...
                        kappa_a,kappa_b,Ekappa,Elnkappa,...
                        a,b,Ealpha, V_a,V_b,lnV,ln1_V, ...
                        phi_z)
                    
occupied=sum(phi_z)>0;

if ~isnan(Ealpha)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % alpha
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    lnpalpha=-1e-6*Ealpha;
    lnqalpha=a*log(b)-gammaln(a)-b*Ealpha;
    lb_alpha=lnpalpha-lnqalpha;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % V_i
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    gamma_1=(sum(phi_z))';
    phi_z2=cumsum(phi_z,2);
    phi_z2=bsxfun(@minus,phi_z2(:,end),phi_z2);
    gamma_2=sum(phi_z2)';
    lnpV=gamma_1.*lnV+gamma_2.*ln1_V+(Ealpha-1)*ln1_V;
    lnqV=gammaln(V_a+V_b)-(gammaln(V_a)+gammaln(V_b))+(V_a-1).*lnV+(V_b-1).*ln1_V;
    lb_V=lnpV-lnqV;
    % lb_V=sum(lb_V(occ_clusters));
    lb_V=sum(lb_V);

    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % z_n
    %%%%%%%%%%%%%%%%%%%%%%%%
    lnqz=phi_z.*log(phi_z);
    lnqz=-sum(lnqz(phi_z>0));
else
    lb_alpha=0;
    lb_V=0;
    lnqz=0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% kappa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if L_flag==1
%     lb_kappa=0;
% else
lnpkappa=(1e-9-1).*Elnkappa-1e-9*Ekappa;
lnqkappa=kappa_a.*log(kappa_b)-gammaln(kappa_a)+(kappa_a-1).*Elnkappa-kappa_b.*Ekappa;
lb_kappa=lnpkappa-lnqkappa;
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%eta
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
d=size(x,2); nu=d/2-1;
sqrt_part=sqrt(eta_kappa.^2+(nu+1).^2);
lb_lnqeta1=sqrt_part+(nu+1)*log(sqrt_part-(nu+1))-2*(nu+1)*log(eta_kappa); %verified

%mu part of q(eta_1)
C=besseli(d/2,eta_kappa)./besseli(d/2-1,eta_kappa);
lb=sqrt(1+((d/2)./eta_kappa).^2)-(d/2)./eta_kappa; 
ub=sqrt(1+((d/2-0.5)./eta_kappa).^2)-(d/2-0.5)./eta_kappa;
C_est=0.5*(lb+ub);
C(isnan(C))=C_est(isnan(C));
lb_lnqeta1=-lb_lnqeta1+eta_kappa.*C;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  p(x_n|z_n)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if level>1
lnpx=bsxfun(@plus,bsxfun(@times,x*Eeta',Ekappa'),(0.5*(d-1)*Elnkappa-Ekappa)').*phi_z;
lnpx=sum(lnpx(:));
% else
%     ;
% end
% lb_z=-sum(lb_z);

L=lnpx-sum(lb_lnqeta1(occupied))+lb_alpha+lb_V+sum(lb_kappa(occupied))+lnqz;
% lik=[L,lnpx,lb_alpha,lb_V,lb_lb_eta,lb_kappa,lnqz];