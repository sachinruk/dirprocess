clear all
close all
clc

numbPointsPerCluster = 200;
noiseRatio = 1;
numbClusters = 4;             %3 cluster, each has unique feature
numbFeaturesPerCluster = 3;   %because total dimension must 3 to plot
                              %total dimension: numbClusters * numbFeaturesPerCluster = 3

[ X, ~ ] = createSyntheticData(numbPointsPerCluster, noiseRatio, numbClusters, numbFeaturesPerCluster);
plotVmf(X,0);
combinedX=[];
mu=[];
for i=1:length(X)
    combinedX=[combinedX; X{i}.data];
    mu=[mu;X{i}.mu];
end
[phi,class,Eeta1]=EM(combinedX);

%generate classes
for i=1:max(class)
    X_clustered{i}.data=combinedX(class==i,:);
    X_clustered{i}.mu=Eeta1(i,:)';
end
figure; plotVmf(X_clustered,1);