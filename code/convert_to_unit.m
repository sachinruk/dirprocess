function uvectors = convert_to_unit(x)
% Convert N vectors to N unit vectors
len=sqrt(sum(x.^2,2));
%remove empty instances
x=x(len>0,:);len=len(len>0);
%get unit vectors
uvectors = bsxfun(@rdivide,x,len);