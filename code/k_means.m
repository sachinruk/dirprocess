function [means,idx]=k_means(x,c,iter,pos)


[~,d]=size(x);
% means=rand_dir(c,d,pos);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%k-means++ algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%
idx=randi(c); %first centre
means=x(idx,:);

for t=1:(c-1)
    dist=1-means*x'; %get distance matrix
    dist = min(dist,[],1);%find points closest to mean vector
    p=dist.^2/sum(dist.^2);
    idx=find(mnrnd(1,p));
    means=[means; x(idx,:)];
end

% delta=0;
for i=1:iter
    dist=1-means*x'; %get distance matrix
    [~,idx] = min(dist);%find points closest to mean vector
    u=unique(idx);%number of clusters n has been assigned to incase some clusters are
    %dropped
    %using those indices create new mean vector
    new_mean=[];
    for j=1:length(u)
        sub_x=x(idx==u(j),:);
        if size(sub_x,1)==1
            mean_x=sub_x;
        else
            mean_x=mean(sub_x);
        end
        try
            new_mean=[new_mean; mean_x];
        catch 
            j=j;
        end
%        disp(j);
    end
%     delta(i)=norm(new_mean(:)-means(:));
    means=new_mean;
    len=sqrt(sum(means.^2,2));
    means=bsxfun(@rdivide,means,len);
end
means=new_mean;

%append a row of random vectors if less than c clusters
if size(means,1)<c
    means=[means; rand_dir(c-size(means,1),size(means,2),pos)];
end
    