function [phi_z,class_best,Eeta_best,llb,class_hierarchy,x2]=EM_ss_nested(x,labels,levels)

train_set=length(labels);

x_test=x((train_set+1):end,:);

iter=100; restarts=40;
lb=zeros(iter,restarts);

L=levels+2;
lb_best=-inf;

Eeta{L}=x;
[N,dim]=size(x);
Eeta{1}=ones(1,dim)/sqrt(dim);

%preallocate memory for parameters
T=zeros(L+1,1)+100;
T(1:3)=1; T(L+1)=N;
Ealpha=cell(L,1);
phi_z=cell(L,1);
Ekappa=cell(L,1);
Elnkappa=cell(L,1);
lnV=cell(L,1);
ln1_V=cell(L,1);
phi_z_train=zeros(train_set,T(L));
phi_z_train(sub2ind(size(phi_z_train), (1:train_set)', labels)) = 1;

a=0; b=0; 

for k=1:restarts
    %initialisations
    Ekappa{L-1}=rand(T(L),1);
    for i=1:L
        Ealpha{i}=10*rand;
        if i==L
            phi_z{i}=[phi_z_train; ones(N-train_set,T(i))/T(i)];
        else
            phi_z{i}=ones(T(i+1),T(i))/T(i);
        end
        if i<L && i>1
            Eeta{i}=abs(randn(T(i+1),dim));
            lenEeta=sum(sqrt(Eeta{i}.^2),2);
            Eeta{i}=bsxfun(@rdivide,Eeta{i},lenEeta);
        end
    end
    i=L-2;
    while i>0
        Ekappa{i}=rand(T(i+1),1)*min(Ekappa{i+1})*1e-2; 
        i=i-1;
    end

    %doing bottom up approach
    for i=1:iter
        level=L; %reset level to bottom
%         lb(i)=0;
        while level>2 % first level of etas are constant parameters
            %get the approximate posteriors
            [Eeta{level-1},eta_kappa]=q_eta(Eeta,Ekappa,phi_z,level-1);                    
            [Ekappa{level-1}, Elnkappa{level-1},kappa_a,kappa_b]=q_kappa(Eeta,phi_z,level-1);
            if size(phi_z{level},2)>1
                [lnV{level}, ln1_V{level},V_a,V_b]=qV(phi_z{level},Ealpha{level});
                [Ealpha{level}, a,b]=q_alpha(ln1_V{level}); 
                if level==L
                    phi_z{level}=qz(lnV{level},ln1_V{level},x_test,...
                                    Eeta{level-1},Ekappa{level-1},Elnkappa{level-1});
                    phi_z{level}=[phi_z_train; phi_z{level}];
                else
                    phi_z{level}=qz(lnV{level},ln1_V{level},Eeta{level},...
                                    Eeta{level-1},Ekappa{level-1},Elnkappa{level-1});
                end
            else
                Ealpha{level}=nan; 
            end
%             if level<L
%                 used_clusters=sum(phi_z{level+1})>0;
%             end
            lb(i,k)=lb(i,k)+lnlb_nCRP(Eeta{level},Eeta{level-1},eta_kappa,...
                kappa_a,kappa_b,Ekappa{level-1},Elnkappa{level-1},...
                a,b,Ealpha{level},...
                V_a,V_b,lnV{level},ln1_V{level},...            
                phi_z{level});

            level=level-1;
        end
        %check for convergence in lower bound
        if i>1
            diff=lb(i,k)-lb(i-1,k);
            if i>15 && abs(diff)<1e-9
                disp(i);
                break;
            end
        end
    end
    if lb(i,k)>lb_best
        lb_best=lb(i,k);
        phi_z_best=phi_z;
        Eeta_best=Eeta;
    end
%     [~,class]=max(phi_z{L},[],2);    
%     subplot(8,5,k);plot(lb(:,k)); xlabel(length(unique(class)));
end

class_best=cell(L,1);
for level=3:L
    [~,class_best{level}]=max(phi_z_best{level},[],2);
%     Eeta_best{level-1}=Eeta_best{level-1}(unique(class_best{level},'stable'),:);
    Eeta_best{level-1}=bsxfun(@rdivide,Eeta_best{level-1},sqrt(sum(Eeta_best{level-1}.^2,2)));
%     [class_best{level},Eeta_best{level-1}]=relabel2(class_best{level},Eeta_best{level-1});
end

level=L-1; k=L-2;
class_hierarchy=zeros(size(x,1),k);
class_hierarchy(:,k)=class_best{L};
while level>2
    k=k-1;
    class_hierarchy(:,k)=class_best{level}(class_hierarchy(:,k+1));    
    level=level-1;
end

u_class=unique(class_hierarchy,'rows');
x2=[];
for i=1:size(u_class,1)
    idx=sum(abs(bsxfun(@minus,u_class(i,:),class_hierarchy)),2)==0;
    x2=[x2; x(idx,:)];
end

llb=max(lb(:));

%vMF approxmiation
function [Eeta,eta_kappa]=q_eta(Eeta,Ekappa,phi_z,level)

kappa_lower=Ekappa{level-1};
eta_lower=Eeta{level-1};

eta_upper=Eeta{level+1};
kappa=Ekappa{level};

phi_z_l=phi_z{level};
phi_z_upper=phi_z{level+1};

v=bsxfun(@times,eta_lower'*phi_z_l',kappa_lower')+...
    bsxfun(@times,eta_upper'*phi_z_upper,kappa');

eta_kappa=sqrt(sum(v.^2));
eta_mu=bsxfun(@rdivide,v,eta_kappa);
d=size(eta_lower,2);
C=besseli(d/2,eta_kappa)./besseli(d/2-1,eta_kappa);

lb=sqrt(1+((d/2)./eta_kappa).^2)-(d/2)./eta_kappa; 
ub=sqrt(1+((d/2-0.5)./eta_kappa).^2)-(d/2-0.5)./eta_kappa;
C_est=0.5*(lb+ub);
C(isnan(C))=C_est(isnan(C));

Eeta=bsxfun(@times,eta_mu,C)';

%gamma approximation on kappa
function [Ekappa, Elnkappa,a,b]=q_kappa(Eeta,phi_z,level)
eta=Eeta{level};
eta_upper=Eeta{level+1};
phi_z_upper=phi_z{level+1};

d=size(eta,2);
a=((d-1)/2)*sum(phi_z_upper)'+1e-9; 
b=sum((1-eta_upper*eta').*phi_z_upper)'+1e-9;

% b=sum((1-Eeta*eta_mu).*phi_z)'; % C2=sum((p'*x)'.*eta_mu)';<- can be used instead
Ekappa=a./b;
Elnkappa=psi(a)-log(b);

%gamma distribution approximation
function [Ealpha, a,b]=q_alpha(ln1_V)
a=length(ln1_V)+1e-6;
b=1e-6-sum(ln1_V);
Ealpha=a/b;
% Elnalpha=psi(a)-log(b);

%beta distribution approximation
function [lnV, ln1_V, gamma_1,gamma_2]=qV(phi_z,Ealpha)
gamma_1=(sum(phi_z)+1)';
phi_z=cumsum(phi_z,2);
phi_z=bsxfun(@minus,phi_z(:,end),phi_z);
gamma_2=(sum(phi_z)+Ealpha)';
% gamma=[gamma_1 gamma_2];

%expectations
lnV=psi(gamma_1)-psi(gamma_1+gamma_2);
ln1_V=psi(gamma_2)-psi(gamma_1+gamma_2);

%multinomail approxmiation
function p=qz(lnV,ln1_V,x,Eeta,Ekappa,Elnkappa)
ln1_V=cumsum(ln1_V);
ln1_V=[0; ln1_V(1:(end-1))];

d=size(x,2);

lnVMF=bsxfun(@plus,bsxfun(@times,x*Eeta',Ekappa'),(0.5*(d-1)*Elnkappa-Ekappa)');

p=bsxfun(@plus,lnVMF,(ln1_V+lnV)');
p=exp(bsxfun(@minus,p,max(p,[],2)));
p=bsxfun(@rdivide, p,sum(p,2));

